package com.guidedarea.android.maps;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Location;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Projection;
import com.guidedarea.R;

public class MyCustomLocationOverlay extends MyLocationOverlay {
	private Context mContext;
	private float mOrientation;
	
	private Paint accuracyPaint;
	private Point center;
	private Point left;
	private Drawable drawable;
	private int width;
	private int height;

	public MyCustomLocationOverlay(Context context, MapView mapView) {
		super(context, mapView);
		mContext = context;
	}

	@Override
	protected void drawMyLocation(Canvas canvas, MapView mapView,
			Location lastFix, GeoPoint myLocation, long when) {
		// translate the GeoPoint to screen pixels
		Point screenPts = mapView.getProjection().toPixels(myLocation, null);
		// create a rotated copy of the marker
		Bitmap arrowBitmap = BitmapFactory.decodeResource(
				mContext.getResources(), R.drawable.arrow);
		mOrientation = lastFix.getBearing();
		Matrix matrix = new Matrix();
		matrix.postRotate(mOrientation);
		Bitmap rotatedBmp = Bitmap.createBitmap(arrowBitmap, 0, 0,
				arrowBitmap.getWidth(), arrowBitmap.getHeight(), matrix, true);
		// add the rotated marker to the canvas
		canvas.drawBitmap(rotatedBmp,
				screenPts.x - (rotatedBmp.getWidth() / 2), screenPts.y
						- (rotatedBmp.getHeight() / 2), null);

		

		accuracyPaint = new Paint();
		accuracyPaint.setAntiAlias(true);
		accuracyPaint.setStrokeWidth(2.0f);
		center = new Point();
		left = new Point();
		Projection projection = mapView.getProjection();
		double latitude = lastFix.getLatitude();
		double longitude = lastFix.getLongitude();
		float accuracy = lastFix.getAccuracy();

		float[] result = new float[1];

		Location.distanceBetween(latitude, longitude, latitude, longitude + 1,
				result);
		float longitudeLineDistance = result[0];

		GeoPoint leftGeo = new GeoPoint((int) (latitude * 1e6),
				(int) ((longitude - accuracy / longitudeLineDistance) * 1e6));
		projection.toPixels(leftGeo, left);
		projection.toPixels(myLocation, center);
		int radius = center.x - left.x;

		accuracyPaint.setColor(0xff6666ff);
		accuracyPaint.setStyle(Style.STROKE);
		canvas.drawCircle(center.x, center.y, radius, accuracyPaint);

		accuracyPaint.setColor(0x186666ff);
		accuracyPaint.setStyle(Style.FILL);
		canvas.drawCircle(center.x, center.y, radius, accuracyPaint);
		
		mapView.postInvalidate();
	}

	public void setOrientation(float newOrientation) {
		mOrientation = newOrientation;
	}
}