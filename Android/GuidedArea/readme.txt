To run you will need your own Google API key.  To obtain this, do the following:

  * Get the MD5 fingerprint using this method;:
    http://code.google.com/android/add-ons/google-apis/mapkey.html#getdebugfingerprint
  * Get a google key using that fingerprint here:
    http://code.google.com/android/maps-api-signup.html
  * Paste that key into the appropriate map xml layouts in eclipse.  These are found
    in SightseeingMapActivity.xml and (Nothingelse).xml inside the MapView layouts

Once you have the API key, you can open the project up in eclipse and run it
on your device or emulator.
