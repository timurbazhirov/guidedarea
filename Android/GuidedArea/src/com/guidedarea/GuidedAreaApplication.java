/*
 * Class that represents the application.
 * Has visited areaobjects (see github documentation) list and defines the distance to them
 * @author Timur Bazhirov, Aug 24, 2012
 * Using GreenDroid, Acra, GoogleAnalytics external library
 */
package com.guidedarea;

import greendroid.app.GDApplication;

import org.acra.*;
import org.acra.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.location.Location;
import android.util.Log;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.google.android.maps.GeoPoint;
import com.guidedarea.android.models.GArea;
import com.guidedarea.android.ui.HomeActivity;
import com.guidedarea.android.utils.TypeConverter;

@ReportsCrashes(formKey = "dDBCN24xVmVqMHl0LTl5UFE5dzNzWHc6MQ") 
public class GuidedAreaApplication extends GDApplication {

	private Location mUserCurrenLocation;

	private static HashMap<GArea, Date> mVisitedGuidedObjects = new HashMap<GArea, Date>();
	
	private static HashMap<String, Date> mVisitedParseObjects = new HashMap<String, Date>();

	private static List<GArea> mGAreas = new ArrayList<GArea>();

	private static String TAG = "######### GUIDED AREA APPLICATION ######### GA_APP";

	private static int TimeInterval = 5; // in minutes

	private GeoPoint startPoint = null;

	private GoogleAnalyticsTracker mTracker = null;
	
	@Override
    public void onCreate() {
        // The following line triggers the initialization of ACRA
        ACRA.init(this);
        super.onCreate();
    }

	public GoogleAnalyticsTracker getTrackerInstance() {
		if (mTracker == null) {
			mTracker = GoogleAnalyticsTracker.getInstance();
			// Start with a dispatch interval (in seconds).
			mTracker.startNewSession("UA-29286090-1", 20, this);
		}
		return mTracker;
	}

	public GeoPoint getStartPoint() {
		return startPoint;
	}

	public void setStartPoint(GeoPoint startPoint) {
		this.startPoint = startPoint;
	}

	@Override
	public Class<?> getHomeActivityClass() {
		return HomeActivity.class;
	}

	public Location getUserCurrenLocation() {
		return mUserCurrenLocation;
	}

	public void setUserCurrenLocation(Location userCurrenLocation) {
		this.mUserCurrenLocation = userCurrenLocation;
	}
/*
	public static HashMap<GArea, Date> getVisitedSites() {
		return mVisitedGuidedObjects;
	}

	public static void setVisitedGuidedObjects(HashMap<GArea, Date> mVisitedSites) {
		mVisitedGuidedObjects = mVisitedSites;
	}
*/
	public static boolean isGuidedObjectVisited(GArea ao) {
		if (mVisitedGuidedObjects.containsKey(ao)) {
			Date d = new Date();
			Date visitedAt = mVisitedGuidedObjects.get(ao);
			Log.i("VISITED", ao.getName() + ", now " + d);
			Log.i("VISITED", ao.getName() + ", visited at  " + visitedAt);
			Log.i("VISITED", ao.getName() + ", diff "
					+ (int) ((d.getTime() - visitedAt.getTime()) / 60000));
			if ((int) ((d.getTime() - visitedAt.getTime()) / 60000) < TimeInterval) {
				return true;
			} else {
				mVisitedGuidedObjects.remove(ao);
				return false;
			}
		}
		else if (mVisitedParseObjects.containsKey(ao.getParseId())){
				Date d = new Date();
				Date visitedAt = mVisitedParseObjects.get(ao.getParseId());
				Log.i("VISITED", ao.getName() + ", now " + d);
				Log.i("VISITED", ao.getName() + ", visited at  " + visitedAt);
				Log.i("VISITED", ao.getName() + ", diff "
					+ (int) ((d.getTime() - visitedAt.getTime()) / 60000));
				if ((int) ((d.getTime() - visitedAt.getTime()) / 60000) < TimeInterval) {
					return true;
				} else {
					mVisitedParseObjects.remove(ao.getParseId());
					return false;
				}
			}
		return false;
	}

	public static void addVisitedGuidedObject(GArea ao) {
		mVisitedGuidedObjects.put(ao, new Date());
		mVisitedParseObjects.put(ao.getParseId(), new Date());
	}

	public static void addAreaObject(GArea a) {
		mGAreas.add(a);
	}
	
	public static void ClearAreaObject() {
		mGAreas.clear();
	}

	public static GArea getNearestGuidedObject(Location loc) {

		GArea nearestGuidedObject = null;

		for (GArea ao : mGAreas) {

			Log.i(TAG, ""
					+ loc.distanceTo(TypeConverter.geoPointToLocation(ao
							.getGeoPoint()))); // Log Debug Print

			// Below compare distances and check if visited

			if ((null == nearestGuidedObject || loc.distanceTo(TypeConverter
					.geoPointToLocation(nearestGuidedObject.getGeoPoint())) > loc
					.distanceTo(TypeConverter.geoPointToLocation(ao
							.getGeoPoint())))
					&& !isGuidedObjectVisited(ao)) {
				nearestGuidedObject = ao;
			}
		}
		return nearestGuidedObject;
	}

	public static GArea getFurthestGuidedObject(Location loc) {

		GArea furthestGuidedObject = null;

		for (GArea ao : mGAreas) {

			Log.i(TAG, ""
					+ loc.distanceTo(TypeConverter.geoPointToLocation(ao
							.getGeoPoint()))); // Log Debug Print

			// Below compare distances and check if visited

			if ((null == furthestGuidedObject || loc.distanceTo(TypeConverter
					.geoPointToLocation(furthestGuidedObject.getGeoPoint())) < loc
					.distanceTo(TypeConverter.geoPointToLocation(ao
							.getGeoPoint())))) {
				furthestGuidedObject = ao;
			}
		}
		return furthestGuidedObject;
	}
			
			
	
}
