/*
 * Class that is used to convert Parse.com objects into the GA type (see "models" directory).
 * Has gareas (see github documentation) and uses JSON handling functions
 * @author Timur Bazhirov, Oct 24, 2012
 */


package com.guidedarea.android.adapters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.location.Location;
import android.util.Log;

import com.google.android.maps.GeoPoint;
import com.guidedarea.android.models.GArea;
import com.guidedarea.android.utils.JsonParser;
import com.parse.ParseException;
import com.parse.ParseObject;

public class ParsetoGA {
	
private static String TAG = "######### GUIDED AREA APPLICATION ######### PARSE_TO_GA "; 

	public static GArea getArea(ParseObject parseobj) throws ParseException,
			JSONException {

		Log.i(ParsetoGA.class.getName(), "getArea(ParseObject parseobj)");

		String description = null, wikiLink = null, photoLink = null, websiteLink = null, address = null, name = null, pointsArray = null, objIdsJson = null;

		int latitude, longitude;
		int id = -1;
		int[] areaObjIds = null;
		String ParseId = null;

		// Required properties

		latitude = (int) (parseobj.getParseGeoPoint("location").getLatitude() * 1000000);
		longitude = (int) (parseobj.getParseGeoPoint("location").getLongitude() * 1000000);

		name = parseobj.getString("name");
		id = parseobj.getInt("thisid");
		ParseId = parseobj.getObjectId();

		Log.i(ParsetoGA.class.getName(),
				TAG + ": Passed name"
						+ name + " " + id + " " + latitude + " " + longitude + " " + new GeoPoint(latitude,
								longitude));

		description = parseobj.getString("description");

		wikiLink = parseobj.getString("wiki_link");

		photoLink = parseobj.getString("photo_link");

		websiteLink = parseobj.getString("website_link");

		address = parseobj.getString("address");

		objIdsJson = parseobj.getString("objects");

		pointsArray = parseobj.getString("points");

		/*
		 * Get Borders depending on perimeter points
		 */

		Location[] border = null;
		if (pointsArray != null) {
			try {
				JSONArray points = new JSONArray(pointsArray);
				border = new Location[points.length()];
				for (int j = 0; j < points.length(); j++) {
					Location loc1 = new Location((String) null);
					JSONObject point = points.getJSONObject(j);
					loc1.setLatitude(point.getDouble("lat"));
					loc1.setLongitude(point.getDouble("long"));
					border[j] = loc1;
				}
			} catch (JSONException e) {
				Log.i(JsonParser.class.getSimpleName(),
						"Error during border points parsing: " + e.toString());
			}
		}

		if (objIdsJson != null) {
			try {
				JSONArray objects = new JSONArray(objIdsJson);
				areaObjIds = new int[objects.length()];
				for (int j = 0; j < objects.length(); j++) {
					JSONObject object = objects.getJSONObject(j);
					areaObjIds[j] = object.getInt("id");
				}
			} catch (JSONException e) {
				Log.i(JsonParser.class.getSimpleName(),
						"Error during convert jsonArray to array");
			}
		}

		GArea ga = (new GArea.Builder(id, name, new GeoPoint(latitude,
				longitude))).ParseId(ParseId).description(description).address(
				address).areaObjIds(areaObjIds).border(border).website(
				websiteLink).wikiLink(wikiLink).photoLink(photoLink).build();

		return ga;

	}
}
