/*
 * Class that listens to the proximity of the objects in the Guide regime and fires up the chosen pop-up: 
 * either native Android or Nuance's text-to-speech
 * @author Timur Bazhirov, Aug 24, 2012
 * Using Nuance external library
 */
package com.guidedarea.android.listeners;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.guidedarea.GuidedAreaApplication;
import com.guidedarea.android.models.GArea;
import com.guidedarea.android.ui.SightseeingMapActivity;
import com.guidedarea.android.utils.NuanceTtsHelper;
import com.guidedarea.android.utils.SvoxTtsHelper;
import com.guidedarea.android.utils.TextToSpeechHelper;
import com.guidedarea.android.utils.TypeConverter;
import com.nuance.nmdp.speechkit.Vocalizer;

public class AudioGuideLocationListener implements GaLocationListener {

	private LocationManager mLocManager;

	private Context mContext;
	private Activity mactivity = null;

	private String mGpsProvider = LocationManager.GPS_PROVIDER;
	private long mGpsUpdateInterval = 1000; // In milliseconds
	private float mGpsMoveInterval = 1; // In meters

	private String TAG = "######### GUIDED AREA APPLICATION ######### AudioGuideLocationListener";

	private SvoxTtsHelper mSvoxTtsHelper;
	private NuanceTtsHelper mNuanceTtsHelper;
	private TextToSpeechHelper mTextToSpeechHelper;

	private int mTtsEngine; // 0 - svox, 1 - nuance, 2 - default

	private int mGuideLaunchDistance; // in meters
	//private boolean isSpeaking = false;

	/********************************************************
	 * Class Builder - for Nuance
	 * 
	 * @param context - context where it is being started
	 * @param ttsEngine - TTS engine type
	 * @param guideLaunchDistance - distance for Guide launch
	 ********************************************************/
/*
	public AudioGuideLocationListener(Context context, int ttsEngine, int guideLaunchDistance) {
		
		// initialize all parameters
		mLocManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		mContext = context;
		mNuanceTtsHelper = NuanceTtsHelper.createInstance(context);
		mTtsEngine = ttsEngine;
		mGuideLaunchDistance = guideLaunchDistance;
	}
*/
	/*********************************************************
	 * Class Builder 2 - For Default TTS
	 * 
	 * @param context
	 * @param ttsEngine
	 * @param guideLaunchDistance
	 * @param activity
	 ********************************************************/

	public AudioGuideLocationListener(Context context, int ttsEngine,
			int guideLaunchDistance, Activity activity, Vocalizer vocalizer) {
		mLocManager = (LocationManager) context
				.getSystemService(Context.LOCATION_SERVICE);
		mContext = context;
		
		mTextToSpeechHelper = TextToSpeechHelper.createInstance(activity);
		mTextToSpeechHelper.startActivityResult();
		
		mNuanceTtsHelper = NuanceTtsHelper.createInstance(context);
		mNuanceTtsHelper.setVocalizer(vocalizer);
		
		mTtsEngine = ttsEngine;
		
		mGuideLaunchDistance = guideLaunchDistance;
		mactivity = activity;
	}

	/**********************************************************
	 * ON location Changed
	 *********************************************************/

	@Override
	public void onLocationChanged(Location location) {

		Log.i(TAG, location.toString());
		
		// Check if No nearest location found		
		
		if (!(GuidedAreaApplication.getNearestGuidedObject(location) == null)) {
			

			// get nearest object

			GArea ao = (GArea) GuidedAreaApplication.getNearestGuidedObject(location);
			
			Log.i(TAG + "Location", ao.toString());

			// Initialize TTS: commented out as it gives force closure when ao = null
			/*
			mSvoxTtsHelper = SvoxTtsHelper.createInstance(ao.getDescription());
		
			if (mactivity!=null){
			mTextToSpeechHelper = TextToSpeechHelper.createInstance(mactivity);
			}		
			 */
			// Calculate distance to nearest object

			float d = location.distanceTo(TypeConverter.geoPointToLocation(ao.getGeoPoint()));

			Log.i(TAG, d + "m to " + ao.getName());

			// Update overlays if we haven't done so in a while
			Location Last = TypeConverter.geoPointToLocation(SightseeingMapActivity.LastDownload);
			
			if (Last.distanceTo(location) >  (location.distanceTo(TypeConverter.geoPointToLocation(ao
					.getGeoPoint()))/1)){
				((SightseeingMapActivity) mactivity).UpdateOverlays();
			}

		/************************************************************************************
		 * THIS NEEDS TO BE UPDATED TO INCLUDE PERIMETER POINTS: Distance check
		 ************************************************************************************/
			// Check if close enough
			if (location.distanceTo(TypeConverter.geoPointToLocation(ao
				.getGeoPoint())) < mGuideLaunchDistance && !GuidedAreaApplication.isGuidedObjectVisited(ao)) {
			
				final String loc_name = ao.getName();
				final String loc_description = ao.getDescription();
				GuidedAreaApplication.addVisitedGuidedObject(ao);
				
				Log.i(TAG, "location name = " + loc_name + " " + "NuanceTTSHelper vocalizer = " + mNuanceTtsHelper.getVocalizer());
				Log.i(TAG, "location name = " + loc_name + " " + "TTSHelper  = " + mTextToSpeechHelper);

			// start TTS depending on the type

				switch (mTtsEngine) {
				case 0:
					if (mNuanceTtsHelper != null && ao != null) {
						//String loc_name = ao.getName();						
						mNuanceTtsHelper.startSpeech("You are nearby " + loc_name + loc_description);
						//mNuanceTtsHelper.startMSpeech("<prosody rate=\"slow\">This is the text which is spoken slow, but the voice sounds distorted/warped/ghastly</prosody>");
					} else {
						//Toast.makeText(mContext, "Problem with Nuance",	Toast.LENGTH_SHORT).show();
						Log.e(TAG + "Problem with Nuance", mContext.toString());
					}
					break;
				case 1:
					if (mTextToSpeechHelper != null && ao != null) {
						
						mTextToSpeechHelper.startSpeech("You are nearby " + loc_name + loc_description, true); // showing StopTTS button - passing true flag
					} else {
						//Toast.makeText(mContext, "Problem with TTS",Toast.LENGTH_SHORT).show();
						Log.e(TAG + "Problem with Default TTS", mContext.toString());
					}
					break;
				}

				Toast.makeText(mContext, String.format( "%.2f", d )  + " m to " + ao.getName(),
					Toast.LENGTH_SHORT).show();
			}
		} 
		else {
			Log.i(TAG + "OnLocationChanged in AudioLocationListener returned null: ", "Error?");
			//Toast.makeText(mContext, "Error: No POI found", Toast.LENGTH_SHORT).show();				
			return;			
		}

		
	}

	/**********************************************************************
	 * DEFAULT METHODS
	 * 
	 * @see android.location.LocationListener#onProviderDisabled(java.lang.String)
	 **********************************************************************/

	@Override
	public void onProviderDisabled(String provider) {
		mLocManager.removeUpdates(this);
	}

	@Override
	public void onProviderEnabled(String provider) {
		mLocManager.requestLocationUpdates(mGpsProvider, mGpsUpdateInterval,
				mGpsMoveInterval, this);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDestroy() {
		if (mSvoxTtsHelper != null) {
			mSvoxTtsHelper.onDestroy();
		}
		if (mTextToSpeechHelper != null) {
			mTextToSpeechHelper.onDestroy();
		}
		if (mNuanceTtsHelper != null) {
			mNuanceTtsHelper.onDestroy();
		}
	}
	/*
	public void TtsSpeak(String string){
	
		if (mTextToSpeechHelper != null) {
			mTextToSpeechHelper.startSpeech(string, true);
		}
	}
	*/

	@Override
	public void Stop() {
		// TODO Auto-generated method stub
		mTextToSpeechHelper.Stop();
		
	}
}
