/*
 * Class that customizes LocationListener
 * For future changes
 * @author Timur Bazhirov, Aug 24, 2012
 * Using Nuance external library
 */

package com.guidedarea.android.listeners;

import android.location.LocationListener;

public interface GaLocationListener extends LocationListener {
	public void onDestroy();

	public void Stop(); //Method that is used to stop native Android TTS in LocListeners, pointing to TtsHelper class's Stop() method
}
