/*
 * Class that listens to the proximity of the objects in the Guide regime and changes the marker of the nearby building overlay
 * @author Timur Bazhirov, Oct 30, 2012
 * 
 */

package com.guidedarea.android.listeners;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.guidedarea.GuidedAreaApplication;
import com.guidedarea.android.maps.GuidedObjectOverlayItem;
import com.guidedarea.android.models.GArea;
import com.guidedarea.android.ui.AreaExploreActivity;
import com.guidedarea.android.ui.SightseeingMapActivity;
import com.guidedarea.android.utils.TypeConverter;

public class SilentGuideLocationListener implements GaLocationListener {
	private LocationManager mLocManager;
	private Context mContext;
	private Activity mActivity;
	private String mGpsProvider = LocationManager.GPS_PROVIDER;
	private long mGpsUpdateInterval = 1000; // In milliseconds
	private float mGpsMoveInterval = 1; // In meters
	private String TAG = "######### GUIDED AREA APPLICATION ######### VisualGuideLocationListener";
	private int mGuideLaunchDistance;

	public SilentGuideLocationListener(Context context, int guideLaunchDistance, Activity activity) {
		mLocManager = (LocationManager) context
				.getSystemService(Context.LOCATION_SERVICE);
		mContext = context;
		mGuideLaunchDistance = guideLaunchDistance;
		mActivity = activity;
	}

	@Override
	public void onLocationChanged(Location location) {
		Log.i(TAG, location.toString());
		GArea ao = (GArea) GuidedAreaApplication
				.getNearestGuidedObject(location);
		
		if (ao == null) {
			return;
		}
		else {

			float d = location.distanceTo(TypeConverter.geoPointToLocation(ao
					.getGeoPoint()));
			Log.i(TAG, d + "m to " + ao.getName());
			
			Location Last = TypeConverter.geoPointToLocation(SightseeingMapActivity.LastDownload);
			
			if (Last.distanceTo(location) >  d/1){
				((SightseeingMapActivity) mActivity).UpdateOverlays();
			}
			
			
			if (location.distanceTo(TypeConverter.geoPointToLocation(ao
					.getGeoPoint())) < mGuideLaunchDistance && !GuidedAreaApplication.isGuidedObjectVisited(ao)) {
				GuidedAreaApplication.addVisitedGuidedObject(ao);
			
				// Update the overlay
				((SightseeingMapActivity) mActivity).SetOverlayFocused(ao);
				
				Toast.makeText(mContext, String.format( "%.2f", d )  + "m to " + ao.getName(),
						Toast.LENGTH_SHORT).show();
			}

		}		
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		mLocManager.removeUpdates(this);
	}

	@Override
	public void onProviderEnabled(String provider) {
		mLocManager.requestLocationUpdates(mGpsProvider, mGpsUpdateInterval,
				mGpsMoveInterval, this);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDestroy() {
	}

	@Override
	public void Stop() {
		// Left blank intentionally		
	}

}
