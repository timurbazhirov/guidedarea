/*
 * Class that customizes ItemizedOverlay to use Gareas
 * @author Timur Bazhirov, Aug 24, 2012
 * Using Nuance external library
 */

package com.guidedarea.android.maps;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.MotionEvent;

import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;
import com.guidedarea.R;
import com.guidedarea.android.models.GArea;
import com.guidedarea.android.ui.SightseeingMapActivity;

public class GuidedObjectItemizedOverlay extends
		ItemizedOverlay<GuidedObjectOverlayItem> {

	private ArrayList<GuidedObjectOverlayItem> mOverlays = new ArrayList<GuidedObjectOverlayItem>();
	private final String TAG = "GuidedObjectItemizedOverlay";
	private Context mContext;

	public GuidedObjectItemizedOverlay(Drawable defaultMarker, Context context) {
		super(boundCenterBottom(defaultMarker));
		mContext = context;
		populate();
	}

	public void setContext(Context context) {
		this.mContext = context;
	}
	
	@Override
	protected GuidedObjectOverlayItem createItem(int i) {
		return mOverlays.get(i);
	}

	@Override
	public int size() {
		return mOverlays.size();
	}

	@Override
	public void draw(android.graphics.Canvas canvas, MapView mapView,
			boolean shadow) {
		super.draw(canvas, mapView, false);
	}

	@Override
	protected boolean onTap(int index) {
		SightseeingMapActivity sa = (SightseeingMapActivity) mContext;
		sa.showObjInfo(mOverlays.get(index).getAreaObject());
	    /*
		GuidedObjectOverlayItem item = mOverlays.get(index);
	    //Get the new Drawable
	    Drawable marker = mContext.getResources().getDrawable(R.drawable.icon);
	    //Set its bounds
	    marker.setBounds(0,0,marker.getIntrinsicWidth(),marker.getIntrinsicHeight());
	    //Set the new marker
	    item.setMarker(marker);
	    */
		return true;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event, MapView mapView) {
		SightseeingMapActivity sa = (SightseeingMapActivity) mContext;
		sa.hideObjInfo();
		return super.onTouchEvent(event, mapView);
	}

	public void addOverlay(GuidedObjectOverlayItem overlayItem) {
		mOverlays.add(overlayItem);
		Log
				.i(TAG, "New Guided Obj has been created: "
						+ overlayItem.getTitle());
		populate();
	}
	
	public int getOverlayItem (GArea a){
		for (int i=0; i< mOverlays.size(); i++){
			if (mOverlays.get(i).getAreaObject().getParseId() == a.getParseId()){
				return i;
			}
				
		}
		return 0;
	}

}
