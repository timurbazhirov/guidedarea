/*
 * Class that customizes OverlayItem to represent Gareas
 * @author Timur Bazhirov, Aug 24, 2012
 * Using Nuance external library
 */

package com.guidedarea.android.maps;

import com.google.android.maps.OverlayItem;
import com.guidedarea.android.models.GArea;

public class GuidedObjectOverlayItem extends OverlayItem {
	private GArea mGuidedObject;

	public GuidedObjectOverlayItem(GArea a) {
		super(a.getGeoPoint(), a.getName(), a.getDescription());
		mGuidedObject = a;
	}

	public GArea getAreaObject() {
		return mGuidedObject;
	}

}
