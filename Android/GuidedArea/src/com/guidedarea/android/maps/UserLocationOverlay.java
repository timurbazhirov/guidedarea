/*
 * Class that customizes MyLocationOverlay
 * For future use
 * @author Timur Bazhirov, Aug 24, 2012
 * Using Nuance external library
 */

package com.guidedarea.android.maps;

import android.content.Context;

import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;

public class UserLocationOverlay extends MyLocationOverlay {


	public UserLocationOverlay(Context context, MapView mapView) {
		super(context, mapView);
	}

}
