/*
 * Class that creates an interface to handle zoom on the map
 * @author Timur Bazhirov, Aug 24, 2012
 * Using Nuance external library
 */

package com.guidedarea.android.maps;

public interface ZoomListener {
	public void onZoom(int oldZoom, int newZoom);
}
