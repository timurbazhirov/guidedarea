package com.guidedarea.android.models;

import android.location.Location;

import com.google.android.maps.GeoPoint;

public class GArea {
	private int id;
	private String name;
	private String wikiLink;
	private String photoLink;
	private String description;
	private String[] photos;
	private String address;
	private Comment[] comments;
	private GeoPoint geoPoint;
	private String website;
	private Location[] border;
	private int[] areaObjIds;

	public static class Builder {
		// Required fields
		private String name;
		private String description;
		private GeoPoint geoPoint;
		
		//Optional fields
		private int id;
		private String wikiLink;
		private String photoLink;
		private String[] photos;
		private String address;
		private Comment[] comments;
		private String website;
		private Location[] border;
		private int[] areaObjIds;

		public Builder(String name, String description, GeoPoint geoPoint) {
			this.name = name;
			this.description = description;
			this.geoPoint = geoPoint;
		}
		public Builder(int id, String name, String description, GeoPoint geoPoint) {
			this.id = id;
			this.name = name;
			this.description = description;
			this.geoPoint = geoPoint;
		}
		public Builder(int id, String name, GeoPoint geoPoint) {
			this.id = id;
			this.name = name;
			this.geoPoint = geoPoint;
		}
		
		public Builder id(int id) {
			this.id = id;
			return this;
		}
		
		public Builder description(String description) {
			this.description = description;
			return this;
		}
		
		public Builder wikiLink(String s) {
			this.wikiLink = s;
			return this;
		}
		
		public Builder photoLink(String s) {
			this.photoLink = s;
			return this;
		}
		
		public Builder photos(String[] s) {
			this.photos = s;
			return this;			
		}
		
		public Builder address(String s) {
			this.address = s;
			return this;
		}
		
		public Builder comments(Comment[] c) {
			this.comments = c;
			return this;
		}
		
		public Builder website(String s){
			this.website = s;
			return this;
		}
		
		public Builder border(Location[] b){
			this.border = b;
			return this;
		}
		
		public Builder areaObjIds(int[] ids){
			this.areaObjIds = ids;
			return this;
		}
		
		public GArea build(){
			return new GArea(this);
		}
		
	}

	private GArea(Builder b) {
		id = b.id;
		name = b.name;
		wikiLink = b.wikiLink;
		photoLink = b.photoLink;
		description = b.description;
		photos = b.photos;
		address = b.address;
		comments = b.comments;
		geoPoint = b.geoPoint;
		website = b.website;
		border = b.border;
		areaObjIds = b.areaObjIds;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWikiLink() {
		return wikiLink;
	}

	public void setWikiLink(String wikiLink) {
		this.wikiLink = wikiLink;
	}
	
	public String getPhotoLink() {
		return photoLink;
	}

	public void setPhotoLink(String photoLink) {
		this.photoLink = photoLink;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String[] getPhotos() {
		return photos;
	}

	public void setPhotos(String[] photos) {
		this.photos = photos;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Comment[] getComments() {
		return comments;
	}

	public void setComments(Comment[] comments) {
		this.comments = comments;
	}

	public GeoPoint getGeoPoint() {
		return geoPoint;
	}

	public void setGeoPoint(GeoPoint geoPoint) {
		this.geoPoint = geoPoint;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public Location[] getBorder() {
		return border;
	}

	public void setBorder(Location[] border) {
		this.border = border;
	}

	public int[] getAreaObjIds() {
		return areaObjIds;
	}

	public void setAreaObjIds(int[] areaObjIds) {
		this.areaObjIds = areaObjIds;
	}

	public int getId() {
		return id;
	}
	
	
}
