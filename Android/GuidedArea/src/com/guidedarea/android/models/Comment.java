/*
 * Model that represents GArea's comments
 * NOT USED CURRENTLY: Should be replaced with Parse.com data
 * @author Timur Bazhirov, Aug 24, 2012
 * Using Nuance external library
 */


package com.guidedarea.android.models;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {

	private static final long serialVersionUID = -1026801155777216337L;
	private int mId;
	private Date mDate;
	private User mUser;
	private String mText;

	public int getmId() {
		return mId;
	}

	public void setmId(int mId) {
		this.mId = mId;
	}

	public Date getmDate() {
		return mDate;
	}

	public void setmDate(Date mDate) {
		this.mDate = mDate;
	}

	public User getmUser() {
		return mUser;
	}

	public void setmUser(User mUser) {
		this.mUser = mUser;
	}

	public String getmText() {
		return mText;
	}

	public void setmText(String mText) {
		this.mText = mText;
	}

}
