/*
 * Model that defines guide type
 * @author Timur Bazhirov, Aug 24, 2012
 * Using Nuance external library
 */

package com.guidedarea.android.models;

public enum GuideType {
	AUDIO, VISUAL;
}
