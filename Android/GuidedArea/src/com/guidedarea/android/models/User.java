/*
 * Model that represents GArea's User fields
 * NOT USED CURRENTLY: Should be replaced with Parse.com data
 * @author Timur Bazhirov, Aug 24, 2012
 * Using Nuance external library
 */


package com.guidedarea.android.models;

public class User {

	public User(String mName) {
		super();
		this.mName = mName;
	}

	private String mName;

	public String getName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

}
