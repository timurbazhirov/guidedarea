/*
 * Class that represents the Sightseeing View Screen for GArea.
 * Shows info and can start audio
 * @author Timur Bazhirov, Sep 5, 2012
 * Using GreenDroid and Parse.com external libraries
 */

package com.guidedarea.android.ui;

import greendroid.app.GDActivity;
import greendroid.widget.ActionBar;
import greendroid.widget.ActionBarItem;
import greendroid.widget.LoaderActionBarItem;
import greendroid.widget.NormalActionBarItem;



import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebChromeClient;
import android.widget.TextView;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.guidedarea.GuidedAreaApplication;
import com.guidedarea.R;
import com.guidedarea.android.utils.MapActivityHelper;
import com.guidedarea.android.utils.TextToSpeechHelper;
import com.guidedarea.android.utils.NuanceTtsHelper;
import com.nuance.nmdp.speechkit.SpeechError;
import com.nuance.nmdp.speechkit.SpeechKit;
import com.nuance.nmdp.speechkit.Vocalizer;


public class AreaExploreActivity extends GDActivity {

	// Initialize: Fields to show
	private TextView areaNameText, areaDescrText, areaAddressText, areaDist;
	private WebView areaPhotoLink;

	// Variables to Show
	// Picture to show
	private Bitmap Drawable;
	private String Name;
	private String Description;
	private String Distance;

	// Log output TAG
	private final String TAG = "######### GUIDED AREA APPLICATION ######### AREA_EXPLORE_ACTIVITY";

	// Nuance TTS Engine and context
	private Vocalizer _vocalizer;
	private Object _lastTtsContext = null;	
	Vocalizer.Listener vocalizerListener;
	
	// Default TTS
	private TextToSpeechHelper TtsHelper;
	private TextToSpeech mTts;

	// Handler for progress bar
	private final Handler mHandler = new Handler();

	// Vocalizer state boolean
	private boolean VocalizerSpeaking = false;

	// Saved state, needed for the orientation change

	private class SavedState {
		String Name;
		String Description;
		String Distance;
		Bitmap Drawable;
		Vocalizer vocalizer;
		Object context;
	}

	// Needed to update the action bar button properly
	private LoaderActionBarItem loaderItem;
	private ActionBarItem localitem;
	
	// help with shared preferences
	private MapActivityHelper mMapActivityHelper;
	private int mTtsEngine;

	// Analytics
	private GoogleAnalyticsTracker mTracker;
	
	/******************************************************
	 * ON CREATE
	 *******************************************************/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setActionBarContentView(R.layout.activity_area_explore);

		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		
		// Google Analytics
		mTracker = ((GuidedAreaApplication)	getApplication()).getTrackerInstance();
		mTracker.trackPageView("/area_explore");
		
		mMapActivityHelper = MapActivityHelper.createInstance(this);


		// init UI elements
		areaPhotoLink = (WebView) findViewById(R.id.photo_image);
		areaNameText = (TextView) findViewById(R.id.area_name);
		areaDescrText = (TextView) findViewById(R.id.text_description);
		areaAddressText = (TextView) findViewById(R.id.text_address);
		areaDist = (TextView) findViewById(R.id.text_distance);

		// Get the Extras from previous activity
		Bundle extras = getIntent().getExtras();
		Name = extras.getString("Name");
		Description = extras.getString("Description");
		Distance = extras.getString("Distance");

		areaNameText.setText(Name);
		areaDescrText.setText(Description);
		areaDist.setText(Distance);

		// WebView + ProgressBar Init

		areaPhotoLink.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				AreaExploreActivity.this.setTitle("  Loading...");
				AreaExploreActivity.this.setProgress(progress * 100);

				if (progress == 100)
					AreaExploreActivity.this.setTitle("");
			}
		});

		areaPhotoLink.setWebViewClient(new WebViewClient() {
			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				// Handle the error
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}
		});

		final String url = extras.getString("PhotoLink");
		
		//String html = String.format("<center><img src=\"%s\">", url);
		String html = String.format("<html><body style=\"text-align: center; background-color: null; vertical-align: center;\"><img src = \"%s\" /></body></html>", url);
		Log.i(TAG, html);
		// areaPhotoLink.getSettings().setLoadWithOverviewMode(true);
		// areaPhotoLink.getSettings().setUseWideViewPort(true);
		areaPhotoLink.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		//areaPhotoLink.getSettings().setUseWideViewPort(false);
		areaPhotoLink.loadData(html, "text/html", null);
		
		
		// initialize TTS type
		initPreferences();
		
		// TTS initialization
		if (mTtsEngine == 1){
			// Create TTS
			TtsHelper = TextToSpeechHelper.createInstance(this);
			TtsHelper.startActivityResult();
			mTts = TtsHelper.getTTS();
		}
		else{
			// Create Vocalizer Listener
			vocalizerListener = createVocalizerListener();
		}
		
		

		// Initialize Action Bar
		initActionBar();
		
		
		/**
		 * If this Activity is being recreated due to a config change (e.g.
		 * screen rotation), check for the saved state.
		 */

		SavedState savedState = (SavedState) getLastNonConfigurationInstance();

		if (savedState == null) {

			// Create a single Vocalizer here and connect in advance
			
			if (mTtsEngine == 0){ // Nuance TTS
				_vocalizer = NuanceTtsHelper.getSpeechKit(this)
						.createVocalizerWithLanguage("en_US", vocalizerListener,
								new Handler());
				_vocalizer.setVoice("Samantha");
	
				NuanceTtsHelper.getSpeechKit(this).connect();
	
				Log.i(TAG, "NUANCE VOCALIZER CREATED");
			}
			
		} else {

			Name = savedState.Name;
			Description = savedState.Description;
			Distance = savedState.Distance;
			Drawable = savedState.Drawable;

			// get the saved state parameters

			areaNameText.setText(Name);
			areaDescrText.setText(Description);
			areaDist.setText(Distance);
			
			if (mTtsEngine == 0){ // Nuance TTS
				_vocalizer = savedState.vocalizer;
				_lastTtsContext = savedState.context;
	
				// Need to update the listener, since the old listener is pointing
				// at the old TtsView activity instance.
	
				_vocalizer.setListener(vocalizerListener);
				
				if (VocalizerSpeaking) showStopButton();
			}
			else if (mTts.isSpeaking()) showStopButton();
		}

		
		// Disabled for now as the touch event was working weird
		areaPhotoLink.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				Log.d(TAG, " WebView Clicked");
				if (arg1.getAction() == MotionEvent.ACTION_DOWN){
					//startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
				}
				return false;
			}
		});
	}

	private Vocalizer.Listener createVocalizerListener(){
				
				// Create Vocalizer listener

				Vocalizer.Listener vocalizerListener = new Vocalizer.Listener() {
					@Override
					public void onSpeakingBegin(Vocalizer vocalizer, String text,
							Object context) {
						VocalizerSpeaking = true;
						showStopButton();
					}

					@Override
					public void onSpeakingDone(Vocalizer vocalizer, String text,
							SpeechError error, Object context) {

						// Use the context to determine if this was the final TTS phrase

						// AreaExploreActivity.this.getActionBar().getItem(0).setDrawable(R.drawable.gd_play_icon);

						VocalizerSpeaking = false;
						showPlayButton();

						if (context != _lastTtsContext) {
							// TODO: change button to PLAY
						} else {
						}
					}
				};
				
		return vocalizerListener;

	}
	
	
	/**********************************************
	 * INIT PREFERENCES FUNCTION - Get Preferences about map view
	 **********************************************/

	private void initPreferences() {
		Log.i(TAG, "initPreferences() called");
		SharedPreferences shPrefs = mMapActivityHelper.getPreferences();
		// types may need to be adjusted
		try {
			mTtsEngine = Integer.valueOf(shPrefs.getString("ttsEngine", "1"));
		} catch (NumberFormatException nfe) {
			System.out.println("Shared preferences. Could not parse " + nfe);
		}
	}
	
	/*****************************************************************
	 * INITIALIZE ACTION BAR
	 *****************************************************************/

	private void initActionBar() {

		// Type Empty - has no "Home" icon

		this.getActionBar().setType(ActionBar.Type.Empty);

		// Add items: play button

		addActionBarItem(
				this.getActionBar().newActionBarItem(NormalActionBarItem.class)
						.setDrawable(R.drawable.gd_play_icon)
						.setContentDescription(R.string.settings),
				R.id.action_bar_info);

		// addActionBarItem(this.getActionBar().newActionBarItem(NormalActionBarItem.class).setDrawable(R.drawable.gd_stop_icon).setContentDescription(R.string.settings),
		// R.id.action_bar_share);

		// Guided Area Logo

		this.getActionBar().setTitle("");
		TextView tv = (TextView) this.findViewById(R.id.gd_action_bar_title);
		tv.setCompoundDrawablesWithIntrinsicBounds(this.getResources()
				.getDrawable(R.drawable.greendroid_application_logo), null,
				null, null);
	}

	/************************************************************************************************
	 * WHEN ACTION BAR IS PRESSED
	 * 
	 * @see greendroid.app.GDActivity#onHandleActionBarItemClick(greendroid.widget.ActionBarItem,
	 *      int)
	 ************************************************************************************************/

	@Override
	public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {

		localitem = item;

		switch (item.getItemId()) {

		// Speak if Action Bar item is pressed

		case R.id.action_bar_info:
	
			if (mTtsEngine == 0){ // Nuance TTS
			
				if (VocalizerSpeaking) {
					_vocalizer.cancel();
					VocalizerSpeaking = false;
					break;
				}
				
				_vocalizer.speakString(Description, _lastTtsContext);
			}
			else {
				
				if ( mTts.isSpeaking() ) {
					mTts.stop();
					showPlayButton();				
					break;
				}
				
				TtsHelper.startSpeech(Description, false); // don't show the Stop Button - passing false flag
			}

			this.getActionBar().removeItem(item);
			
			addActionBarItem(
					this.getActionBar()
							.newActionBarItem(LoaderActionBarItem.class)
							.setDrawable(R.drawable.gd_play_icon)
							.setContentDescription(R.string.settings),
					R.id.action_bar_info);

			loaderItem = (LoaderActionBarItem) this.getActionBar().getItem(0);
			loaderItem.setLoading(true);

			if (mTtsEngine == 1) showStopButton();
			
			break;			

			// this.getActionBar().removeItem(item);

			/*
			 * addActionBarItem(this.getActionBar().newActionBarItem(
			 * NormalActionBarItem.class).setDrawable(R.drawable.gd_play_icon)
			 * .setContentDescription(R.string.settings),
			 * R.id.action_bar_share);
			 */


		default:
			return super.onHandleActionBarItemClick(item, position);

		}
		return true;

	}
	
	public void showPlayButton() {
		
		AreaExploreActivity.this.getActionBar().removeItem(0);
		addActionBarItem(
				AreaExploreActivity.this.getActionBar().newActionBarItem(NormalActionBarItem.class)
						.setDrawable(R.drawable.gd_play_icon)
						.setContentDescription(R.string.settings),
				R.id.action_bar_info);
	}
	
	public void showStopButton() {
		
		if (loaderItem != null) {
			loaderItem.setLoading(false);
			AreaExploreActivity.this.getActionBar().removeItem(0);
			addActionBarItem(
					AreaExploreActivity.this.getActionBar().newActionBarItem(NormalActionBarItem.class)
							.setDrawable(R.drawable.gd_stop_icon)
							.setContentDescription(R.string.settings),
					R.id.action_bar_info);
		}
	}

	/**************************************************************************************************
	 * DOWNLOAD THE BITMAP USING THE PROGRESS BAR - Not used currently
	 **************************************************************************************************/
/*
	private class GetBitmapTask extends AsyncTask<String, Void, Bitmap> {

		private ProgressDialog progress;

		// Builder

		public GetBitmapTask(ProgressDialog progress) {
			this.progress = progress;
		}

		// Show progress bar

		public void onPreExecute() {
			progress.show();
		}

		// Download the info

		@Override
		protected Bitmap doInBackground(String... id) {

			Bitmap bm = getImageBitmap(id[0]);

			return bm;
		}

		// After download - update fields
		@Override
		protected void onPostExecute(Bitmap result) {
			// TODO Auto-generated method stub
			Drawable = result;
			progress.dismiss();

			InitViewFields(Name, Description, result);

		}

	}
*/
	/**************************************************************************************************
	 * INITIALIZE THE VIEW FIELDS
	 **************************************************************************************************/

	protected void InitViewFields(String Name, String Description, Bitmap bitmap) {

		if (areaNameText != null && areaDescrText != null) {

			areaNameText.setText(Name);
			areaDescrText.setText(Description);

			// Set ImageView to Bitmap
			/*
			 * if (bitmap == null) { //((View)
			 * areaPhotoLink.getParent()).setVisibility(View.GONE);
			 * areaPhotoLink
			 * .setBackgroundDrawable(AreaExploreActivity.this.getResources
			 * ().getDrawable(R.drawable.loading_splash)); } else {
			 * areaPhotoLink.setImageBitmap(bitmap); }
			 */
			// Google Analytics track

			// mTracker.trackPageView("/area_explore/" +
			// mArea.getName().replaceAll(" ", ""));

			// Set Address - Not yet implemented
			/*
			 * if (mArea.getAddress() == null || mArea.getAddress().equals(""))
			 * { ((View) areaAddressText.getParent()) .setVisibility(View.GONE);
			 * } else { areaAddressText.setText(mArea.getAddress()); }
			 */
		} else {
			areaNameText.setText("No Name");
			areaDescrText.setText("No description currently available");
			//areaPhotoLink.setBackgroundDrawable(AreaExploreActivity.this.getResources().getDrawable(R.drawable.loading_splash));
		}
	}

	/****************************
	 * ONDESTROY
	 ****************************/

	@Override
	protected void onDestroy() {
		
		//if (mTts != null) { mTts.stop();}
		
		super.onDestroy();

		SpeechKit SpeechKit;
		SpeechKit = NuanceTtsHelper.getSpeechKit(this);

		if (SpeechKit != null) {

			if (_vocalizer != null) {
				_vocalizer.cancel();
				_vocalizer = null;
			}
		}
		
		mTracker.stopSession();
	}

	
	  @Override public void onBackPressed() {
		  super.onBackPressed();
	
		  //if (_vocalizer != null) { _vocalizer.cancel(); _vocalizer = null; }
		 
		  if ( mTts != null) { 
			  if (mTts.isSpeaking()) {
				  	mTts.stop();
			  }
		  }
	  
	 }
	 
	/***********************************
	 * ON ROTATION/CONFIGURATION CHANGE
	 ***********************************/

	@Override
	public Object onRetainNonConfigurationInstance() {
		// Save the Vocalizer state, because we know the Activity will be
		// immediately recreated.

		// Bundle b = getIntent().getExtras();

		SavedState savedState = new SavedState();
		savedState.Name = Name;
		savedState.Description = Description;
		savedState.Distance = Distance;
		savedState.Drawable = Drawable;
		savedState.vocalizer = _vocalizer;
		savedState.context = _lastTtsContext;

		_vocalizer = null; // Prevent onDestroy() from canceling
		return savedState;
	}

	/***********************************
	 * BITMAP DOWNLOADER
	 ***********************************/
/*
	private Bitmap getImageBitmap(String url) {
		Bitmap bm = null;
		try {
			URL aURL = new URL(url);
			URLConnection conn = aURL.openConnection();
			conn.connect();
			InputStream is = conn.getInputStream();
			BufferedInputStream bis = new BufferedInputStream(is);
			bm = BitmapFactory.decodeStream(bis);
			bis.close();
			is.close();
		} catch (IOException e) {
			Log.e("PHOTO_URL TRANSFER", "Error getting bitmap", e);
		}

		return bm;
	}
*/
	/***********************************
	 * WAY TOSCALE IMAGE FOR THE WEBVIEW
	 ***********************************/
/*
	private int getScale() {
		Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE))
				.getDefaultDisplay();
		int width = display.getWidth();
		Double val = new Double(width) / new Double(100);
		val = val * 100d;
		return val.intValue();
	}
*/
}
