package com.guidedarea.android.ui;

/*
 * The application needs to have the permission to write to external storage
 * if the output file is written to the external storage, and also the
 * permission to record audio. These permissions must be set in the
 * application's AndroidManifest.xml file, with something like:
 *
 * <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
 * <uses-permission android:name="android.permission.RECORD_AUDIO" />
 *
 */
import android.app.Activity;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Bundle;
import android.os.Environment;
import android.view.ViewGroup;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaRecorder;
import android.media.MediaPlayer;

import greendroid.app.GDActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;

import com.guidedarea.R;
import com.guidedarea.android.utils.IOUtil;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ProgressCallback;
import com.parse.SaveCallback;
import com.parse.facebook.FacebookError;
import com.parse.facebook.Util;


public class CopyOfFeedbackActivity extends Activity
{
    private static final String LOG_TAG = "AudioRecordTest";
    private static String mFileName = null;

    private RecordButton mRecordButton = null;
    private MediaRecorder mRecorder = null;

    private PlayButton   mPlayButton = null;
    private MediaPlayer   mPlayer = null;
    
    private SendButton   mSendButton = null;
    private TextView TimerTextView;
    private Timer timer;
    
    boolean mStartRecording;
    boolean mStartPlaying;
    boolean mStartSending;

    private void onRecord(boolean start) {
        if (start) {
 
            timer = new Timer();
            timer.schedule(new StartTimer(System.currentTimeMillis(),TimerTextView), 0,5000);
            startRecording();
            
        } else {
        	
            timer.cancel();
            timer.purge();
            stopRecording();

        }
    }

    private void onPlay(boolean start) {
        if (start) {
            startPlaying();
        } else {
            stopPlaying();
        }
    }

    private void startPlaying() {
        mPlayer = new MediaPlayer();
        
        mPlayer.setOnCompletionListener(new
        		OnCompletionListener() {

        		@Override
        		public void onCompletion(MediaPlayer arg0) {
	    			mPlayButton.setText("Start Playing");
	    			mPlayButton.revert_flag(); 

        		}
        });
        
        try {
            mPlayer.setDataSource(mFileName);
            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {
            Log.e(LOG_TAG, "startPlaying() failed");
        }
    }

    private void stopPlaying() {
        mPlayer.release();
        mPlayer = null;
    }

    private void startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        init_file();
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
            mRecorder.start();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }


    }

    private void stopRecording() {
        
        try {
        	mRecorder.stop();
            mRecorder.release();
            mRecorder = null;
        } catch (IllegalStateException e) {
            Log.e(LOG_TAG, "mRecorder Stop: IllegalStateException!!!");
        }    	

    }
    
  

    public class RecordButton extends Button {
        boolean mStartRecording = true;

        OnClickListener clicker = new OnClickListener() {
            public void onClick(View v) {
                onRecord(mStartRecording);
                if (mStartRecording) {
                    setText("Stop recording");
                } else {
                    setText("Start recording");
                }
                mStartRecording = !mStartRecording;
            }
        };

        public RecordButton(Context ctx) {
            super(ctx);
            setText("Start recording");
            setOnClickListener(clicker);
        }
        public RecordButton(Context ctx, AttributeSet attrs) {
            super(ctx, attrs);
            setText("Send Feedback");
            setOnClickListener(clicker);
        }
    }

    public class PlayButton extends Button {
        boolean mStartPlaying = true;

        OnClickListener clicker = new OnClickListener() {
            public void onClick(View v) {
                onPlay(mStartPlaying);
                if (mStartPlaying) {
                    setText("Stop playing");
                } else {
                    setText("Start playing");
                }
                mStartPlaying = !mStartPlaying;
            }
        };
        
        public void revert_flag() {
        	mStartPlaying = !mStartPlaying;
        }
        
        public PlayButton(Context ctx) {
            super(ctx);
            setText("Start playing");
            setOnClickListener(clicker);
        }
        public PlayButton(Context ctx, AttributeSet attrs) {
            super(ctx, attrs);
            setText("Send Feedback");
            setOnClickListener(clicker);
        }
    }

    
    public class SendButton extends Button {
        boolean mStartPlaying = true;

        OnClickListener clicker = new OnClickListener() {
            public void onClick(View v) {
                onSend(mStartPlaying);
                if (mStartPlaying) {
                    setText("Sending..");
                } else {
                    setText("Send Feedback");
                }
                mStartPlaying = !mStartPlaying;
            }
        };

        public void revert_flag() {
        	mStartPlaying = !mStartPlaying;
        }
        
        public SendButton(Context ctx) {
            super(ctx);
            setText("Send Feedback");
            setOnClickListener(clicker);
        }
        public SendButton(Context ctx, AttributeSet attrs) {
            super(ctx, attrs);
            setText("Send Feedback");
            setOnClickListener(clicker);
        }
    }
    
    private void onSend(boolean start) {
        if (start) {
            startSending();
        } else {
            stopSending();
        }
    }

    private void startSending() {	
    	
     	try {
     		
     		byte [] data = IOUtil.readFile(mFileName);
     		
            Log.d(LOG_TAG, data.toString());
      	    
        	final ParseFile file = new ParseFile("feedback.3gp", data);       	 

	    	file.saveInBackground(new SaveCallback() {
	    		@Override  
	    		public void done(ParseException e) {
	    	    // Handle success or failure here ...
	 
	    			ParseObject Feedback = new ParseObject("Feedback");
	    			
	    			// Get User Facebook Name
	    			final com.parse.facebook.Facebook mFacebook;
	    			mFacebook = ParseFacebookUtils.getFacebook();
	    			String jsonUser, facebookId = null, name = null;    			
	    			
					try {
						jsonUser = mFacebook.request("me");
						JSONObject obj = Util.parseJson(jsonUser);
		    			facebookId = obj.optString("id");
		    			name = obj.optString("name");
					} catch (MalformedURLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (FacebookError e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	    			   			
	    			
					// Create Parse object and save it
	    			Feedback.put("Name", name);
	    			Feedback.put("FacebookID", facebookId);
	    			Feedback.put("audio", file);
	    			Feedback.saveEventually();
	    			
	    			Toast.makeText(getApplicationContext(), "Thanks!", Toast.LENGTH_LONG).show();
	    			
	    			// Update Button text
	    			mSendButton.setText("Send Feedback");
	    			mSendButton.revert_flag();
	    		}
	
	    	}, new ProgressCallback() {
	    	  public void done(Integer percentDone) {
	    	    // Update your progress spinner here. percentDone will be between 0 and 100.
	    		  CopyOfFeedbackActivity.this.setTitle("  Loading...");
				  CopyOfFeedbackActivity.this.setProgress(percentDone);
	    	  }
	    	});
        
     	}
     	
     	catch (NullPointerException e) {
     		Toast.makeText(getApplicationContext(), "ERROR: No recording file found!", Toast.LENGTH_LONG).show();
     		return;
     	} 
     	
     	catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
     		Toast.makeText(getApplicationContext(), "ERROR: No recording file found!", Toast.LENGTH_LONG).show();
			e1.printStackTrace();
		} 
     	
     	catch (IOException e1) {
			// TODO Auto-generated catch block
     		Toast.makeText(getApplicationContext(), "ERROR: No recording file found!", Toast.LENGTH_LONG).show();
			e1.printStackTrace();
		}
     	
    }

    private void stopSending() {
    	Toast.makeText(getApplicationContext(), "Stopped!", Toast.LENGTH_LONG).show();
    }
    
    
    
    
    
    
    public void init_file() {
        //mFileName = this.getApplicationContext().getFilesDir() + "/Feedback/";
    	//mFileName += "feedback.3gp";
        //File file = new File(mFileName);
        //file.mkdirs();
    	
    	// Two lines below DO work, but writing to root dir might not be available without rooting
        mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
        mFileName += "/guided_area_feedback.3gp";
        Log.d(LOG_TAG, "initFile()" + mFileName);
    }

    
    
    
    /***********************************************************
     * TIMER TASK
     * @author timur
     *
     ***********************************************************/
    
    //tells activity to run on ui thread
    class StartTimer extends TimerTask {
    	
    	long starttime;
    	TextView text;
    	
         @Override
         public void run() {
             CopyOfFeedbackActivity.this.runOnUiThread(new Runnable() {

                 @Override
                 public void run() {
                    long millis = System.currentTimeMillis() - starttime;
                    int seconds = (int) (millis / 1000);
                    int minutes = seconds / 60;
                    seconds     = seconds % 60;

                    text.setText(String.format("%d:%02d", minutes, seconds));
                 }
             });
             
             stopRecording();
         }
         
         public StartTimer(long starttime, TextView text) {
        	 this.starttime = starttime;
        	 this.text = text;
         }
    };
    
    

    /***********************************************************
     * 					
     * 						ONCREATE
     *
     ***********************************************************/
    
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

		this.setContentView(R.layout.activity_feedback);
		
		TimerTextView = (TextView) findViewById(R.id.timer);
        
		//mRecordButton = (RecordButton) new RecordButton(this);
		mRecordButton = (RecordButton) findViewById(R.id.ButtonRecord);
        //mPlayButton = (PlayButton) new PlayButton(this);
        mPlayButton = (PlayButton) findViewById(R.id.ButtonPlay);
        //mSendButton = (SendButton) new SendButton(this);
        mSendButton = (SendButton) findViewById(R.id.ButtonSend);
        //LinearLayout ll = new LinearLayout(this);
        /*ll.addView(mRecordButton,
            new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                0));
        */
        /*ll.addView(mPlayButton,
            new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                0));
        */
        /*ll.addView(mSendButton,
            new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                0));
        */
        //setContentView(ll);
    }

    @Override
    public void onPause() {
        super.onPause(); 
        if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
        }

        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
    }
}
