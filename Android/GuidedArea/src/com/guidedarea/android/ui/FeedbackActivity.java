package com.guidedarea.android.ui;

/*
 * The application needs to have the permission to write to external storage
 * if the output file is written to the external storage, and also the
 * permission to record audio. These permissions must be set in the
 * application's AndroidManifest.xml file, with something like:
 *
 * <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
 * <uses-permission android:name="android.permission.RECORD_AUDIO" />
 *
 */
import android.app.Activity;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.view.ViewGroup;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaRecorder;
import android.media.MediaPlayer;

import greendroid.app.GDActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Timer;
import java.util.TimerTask;


import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;

import com.guidedarea.R;
import com.guidedarea.android.utils.IOUtil;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ProgressCallback;
import com.parse.SaveCallback;
import com.parse.facebook.FacebookError;
import com.parse.facebook.Util;


public class FeedbackActivity extends Activity
{
    private static final String LOG_TAG = "AudioRecordTest";
    private static String mFileName = null;

    private Button mRecordButton = null;
    private MediaRecorder mRecorder = null;

    private Button   mPlayButton = null;
    private MediaPlayer   mPlayer = null;
    
    private Button   mSendButton = null;
    private TextView TimerTextView;
    private Timer timer;
    
    boolean mStartRecording;
    boolean mStartPlaying;
    boolean mStartSending;
    
    private long starttime;

    private void onRecord(boolean start) {
        if (start) {
 
            timer = new Timer();
            starttime = System.currentTimeMillis();
            timer.schedule(new firstTask(), 0,500);
            startRecording();
            
        } else {
        	
            timer.cancel();
            timer.purge();
            stopRecording();

        }
    }

    private void onPlay(boolean start) {
        if (start) {
            startPlaying();
        } else {
            stopPlaying();
        }
    }

    private void startPlaying() {
        mPlayer = new MediaPlayer();
        
        mPlayer.setOnCompletionListener(new
        		OnCompletionListener() {

        		@Override
        		public void onCompletion(MediaPlayer arg0) {
	    			mPlayButton.setText("Start Playing");
	    			play_revert_flag(); 

        		}
        });
        
        try {
            mPlayer.setDataSource(mFileName);
            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {
            Log.e(LOG_TAG, "startPlaying() failed");
        }
    }

    private void stopPlaying() {
        mPlayer.release();
        mPlayer = null;
    }

    private void startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        init_file();
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
            mRecorder.start();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }


    }

    private void stopRecording() {
        
        try {
        	mRecorder.stop();
            mRecorder.release();
            mRecorder = null;
        } catch (IllegalStateException e) {
            Log.e(LOG_TAG, "mRecorder Stop: IllegalStateException!!!");
        }    	

    }
    
    
    public class SendButton extends Button {
        boolean mStartPlaying = true;

        OnClickListener clicker = new OnClickListener() {
            public void onClick(View v) {
                onSend(mStartPlaying);
                if (mStartPlaying) {
                    setText("Sending..");
                } else {
                    setText("Send Feedback");
                }
                mStartPlaying = !mStartPlaying;
            }
        };

        public void revert_flag() {
        	mStartPlaying = !mStartPlaying;
        }
        
        public SendButton(Context ctx) {
            super(ctx);
            setText("Send Feedback");
            setOnClickListener(clicker);
        }
        public SendButton(Context ctx, AttributeSet attrs) {
            super(ctx, attrs);
            setText("Send Feedback");
            setOnClickListener(clicker);
        }
    }
    
    private void onSend(boolean start) {
        if (start) {
            startSending();
        } else {
            stopSending();
        }
    }

    private void startSending() {	
    	
     	try {
     		
     		byte [] data = IOUtil.readFile(mFileName);
     		
            Log.d(LOG_TAG, data.toString());
      	    
        	final ParseFile file = new ParseFile("feedback.3gp", data);       	 

	    	file.saveInBackground(new SaveCallback() {
	    		@Override  
	    		public void done(ParseException e) {
	    	    // Handle success or failure here ...
	 
	    			ParseObject Feedback = new ParseObject("Feedback");
	    			
	    			// Get User Facebook Name
	    			final com.parse.facebook.Facebook mFacebook;
	    			mFacebook = ParseFacebookUtils.getFacebook();
	    			String jsonUser, facebookId = null, name = null;    			
	    			
					try {
						jsonUser = mFacebook.request("me");
						JSONObject obj = Util.parseJson(jsonUser);
		    			facebookId = obj.optString("id");
		    			name = obj.optString("name");
					} catch (MalformedURLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (FacebookError e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	    			   			
	    			
					// Create Parse object and save it
	    			Feedback.put("Name", name);
	    			Feedback.put("FacebookID", facebookId);
	    			Feedback.put("audio", file);
	    			Feedback.saveEventually();
	    			
	    			Toast.makeText(getApplicationContext(), "Thanks!", Toast.LENGTH_LONG).show();
	    			
	    			// Update Button text
	    			mSendButton.setText("Send Feedback");
	    			send_revert_flag();
	    		}
	
	    	}, new ProgressCallback() {
	    	  public void done(Integer percentDone) {
	    	    // Update your progress spinner here. percentDone will be between 0 and 100.
	    		  FeedbackActivity.this.setTitle("  Loading...");
				  FeedbackActivity.this.setProgress(percentDone);
	    	  }
	    	});
        
     	}
     	
     	catch (NullPointerException e) {
     		Toast.makeText(getApplicationContext(), "ERROR: No recording file found!", Toast.LENGTH_LONG).show();
     		return;
     	} 
     	
     	catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
     		Toast.makeText(getApplicationContext(), "ERROR: No recording file found!", Toast.LENGTH_LONG).show();
			e1.printStackTrace();
		} 
     	
     	catch (IOException e1) {
			// TODO Auto-generated catch block
     		Toast.makeText(getApplicationContext(), "ERROR: No recording file found!", Toast.LENGTH_LONG).show();
			e1.printStackTrace();
		}
     	
    }

    private void stopSending() {
    	Toast.makeText(getApplicationContext(), "Stopped!", Toast.LENGTH_LONG).show();
    }
    
    
    
    
    
    
    public void init_file() {
        //mFileName = this.getApplicationContext().getFilesDir() + "/Feedback/";
    	//mFileName += "feedback.3gp";
        //File file = new File(mFileName);
        //file.mkdirs();
    	
    	// Two lines below DO work, but writing to root dir might not be available without rooting
        mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
        mFileName += "/guided_area_feedback.3gp";
        Log.d(LOG_TAG, "initFile()" + mFileName);
    }

    
    
    
    /***********************************************************
     * TIMER TASK
     * @author timur
     *
     ***********************************************************/
    
    //this  posts a message to the main thread from our timertask
    //and updates the textfield
   final Handler h = new Handler(new Callback() {

        @Override
        public boolean handleMessage(Message msg) {

		long millis = System.currentTimeMillis() - starttime ;
           int seconds = (int) (millis / 1000);
           int minutes = seconds / 60;
           seconds     = seconds % 60;

           TimerTextView.setText(String.format("%d:%02d", minutes, seconds));
           if (seconds > 29) {
        	   
                onRecord(false);
   				mRecordButton.setText("Start Recording");
   				record_revert_flag();
           }
           return false;
        }
    });
    
   
   //tells handler to send a message
   class firstTask extends TimerTask {

        @Override
        public void run() {
            h.sendEmptyMessage(0);
        }
        
   };
    
  
   public void record_revert_flag() {
   	mStartRecording = !mStartRecording;
   }
   
    public void play_revert_flag() {
    	mStartPlaying = !mStartPlaying;
    }
    
    public void send_revert_flag() {
    	mStartPlaying = !mStartPlaying;
    }

    /***********************************************************
     * 					
     * 						ONCREATE
     *
     ***********************************************************/
    
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

		this.setContentView(R.layout.activity_feedback);
		
		TimerTextView = (TextView) findViewById(R.id.timer);
        
		//mRecordButton 
		
		mRecordButton = (Button) findViewById(R.id.ButtonRecord);
		
		mStartRecording = true;

        OnClickListener clicker1 = new OnClickListener() {
            public void onClick(View v) {
                onRecord(mStartRecording);
                if (mStartRecording) {
                    mRecordButton.setText("Stop recording");
                } else {
                	mRecordButton.setText("Start recording");
                }
                mStartRecording = !mStartRecording;
            }
        };

        mRecordButton.setText("Start recording");
        mRecordButton.setOnClickListener(clicker1);

		
        //mPlayButton 
        mPlayButton = (Button) findViewById(R.id.ButtonPlay);
        
        mStartPlaying = true;

        OnClickListener clicker2 = new OnClickListener() {
            public void onClick(View v) {
                onPlay(mStartPlaying);
                if (mStartPlaying) {
                	mPlayButton.setText("Stop playing");
                } else {
                	mPlayButton.setText("Start playing");
                }
                mStartPlaying = !mStartPlaying;
            }
        };

        mPlayButton.setText("Start playing");
        mPlayButton.setOnClickListener(clicker2);
        
        //mSendButton 
        mSendButton = (Button) findViewById(R.id.ButtonSend);
        
        mStartSending = true;

        OnClickListener clicker3 = new OnClickListener() {
            public void onClick(View v) {
                onSend(mStartPlaying);
                if (mStartPlaying) {
                	mSendButton.setText("Sending..");
                } else {
                	mSendButton.setText("Send Feedback");
                }
                mStartPlaying = !mStartPlaying;
            }
        };

        mSendButton.setText("Send Feedback");
        mSendButton.setOnClickListener(clicker3);

    }

    @Override
    public void onPause() {
        super.onPause(); 
        if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
        }

        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
    }
}
