/*
 * Class that represents the Dashboard Screen
 * Has four buttons and needs to shut down the app if back is pressed from the Dashboard screen - made through setResult
 * @author Timur Bazhirov, Mar 26, 2012
 * Using Google's Dashboard layout
 * Commented all google analytics as it was giving warnings
 */

package com.guidedarea.android.ui;

import greendroid.app.GDActivity;
import greendroid.widget.ActionBarItem;
import greendroid.widget.LoaderActionBarItem;
import greendroid.widget.NormalActionBarItem;
import greendroid.widget.ActionBarItem.Type;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.guidedarea.GuidedAreaApplication;
import com.guidedarea.R;
import com.guidedarea.android.utils.ActivityHelper;
public class HomeActivity extends GDActivity {

	private Intent mIntent;
	private ActivityHelper mActivityHelper;
	private GoogleAnalyticsTracker mTracker;
	private Context mContext;
	private boolean surveyPlacesVoted;
	private boolean surveyToursVoted;
	int requestCode;

	public static final String PREFS_NAME = "GA_PREFS";

	// Preferences

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//Initializing action Bar		
		setActionBarContentView(R.layout.activity_home);
		initActionBar();

		//Creating activity
		mActivityHelper = ActivityHelper.createInstance(this);
		
	/****************************************
	 * BYPASSING THIS ACTIVITY SCREEN FOR NOW
	 ****************************************/
		mIntent = new Intent(HomeActivity.this,
				SightseeingMapActivity.class);
		startActivityForResult(mIntent,requestCode);
		
		
		mTracker = ((GuidedAreaApplication) getApplication()).getTrackerInstance(); // Google Analytics Initialization
		mContext = this;
		mTracker.trackPageView("/home");

		//Survey parameters Initialization
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		surveyPlacesVoted = settings.getBoolean("surveyPlacesVoted", false);
		surveyToursVoted = settings.getBoolean("surveyToursVoted", false);

		/*************************************************
		 * Button Guide
		 *************************************************/

		Button guideButton = (Button) findViewById(R.id.dashboard_btn_guide);
		guideButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// REMOVE COMMENT TAGS LATER mTracker.trackPageView("/guide");
				mIntent = new Intent(HomeActivity.this,
						SightseeingMapActivity.class);
				startActivity(mIntent);
			}
		});

		/*************************************************
		 * Button Travel
		 *************************************************/
		
		//Now opens a Dialog for voting. After voting shows "Not available" 
		Button travelButton = (Button) findViewById(R.id.dashboard_btn_travel);
		travelButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// REMOVE COMMENT TAGS LATER mTracker.trackPageView("/tours");
				if (!surveyToursVoted) {
					final Dialog dialog = new Dialog(mContext);

					dialog.setContentView(R.layout.dialog_tours);
					dialog.setTitle("User survey");
					dialog.show();
					
					RadioButton rbA = (RadioButton) (dialog
							.findViewById(R.id.radioA));
					rbA.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// REMOVE COMMENT TAGS LATER mTracker.trackPageView("/survey/tours/a");
							vote(0);
							dialog.cancel();
						}
					});
					
					RadioButton rbB = (RadioButton) (dialog
							.findViewById(R.id.radioB));
					rbB.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// REMOVE COMMENT TAGS LATER mTracker.trackPageView("/survey/tours/b");
							vote(0);
							dialog.cancel();
						}
					});
					
					RadioButton rbC = (RadioButton) (dialog
							.findViewById(R.id.radioC));
					rbC.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// REMOVE COMMENT TAGS LATER mTracker.trackPageView("/survey/tours/c");
							vote(0);
							dialog.cancel();
						}
					});
				} else {
					Toast.makeText(mContext, R.string.disabled_feature,
							Toast.LENGTH_SHORT).show();
				}
				// mActivityHelper.startActivity(ToursListActivity.class);
			}
		});

		/*************************************************
		 * Button Places
		 *************************************************/
		
		//Now opens a Dialog for voting. After voting shows "Not available"
		Button placesButton = (Button) findViewById(R.id.dashboard_btn_places);
		placesButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// REMOVE COMMENT TAGS LATER mTracker.trackPageView("/places");
				if (!surveyPlacesVoted) {
					final Dialog dialog = new Dialog(mContext);

					dialog.setContentView(R.layout.dialog_places);
					dialog.setTitle("User survey");
					dialog.show();
					
					RadioButton rbA = (RadioButton) (dialog
							.findViewById(R.id.radioA));
					rbA.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// REMOVE COMMENT TAGS LATER mTracker.trackPageView("/survey/places/a");
							vote(1);
							dialog.cancel();
						}
					});
					
					RadioButton rbB = (RadioButton) (dialog
							.findViewById(R.id.radioB));
					rbB.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// REMOVE COMMENT TAGS LATER mTracker.trackPageView("/survey/places/b");
							vote(1);
							dialog.cancel();
						}
					});
					
					RadioButton rbC = (RadioButton) (dialog
							.findViewById(R.id.radioC));
					rbC.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// REMOVE COMMENT TAGS LATER mTracker.trackPageView("/survey/places/c");
							vote(1);
							dialog.cancel();
						}
					});
				} else {
					Toast.makeText(mContext, R.string.disabled_feature,
							Toast.LENGTH_SHORT).show();
				}
				// mActivityHelper.startActivity(PlacesMapActivity.class);
			}
		});

		/*************************************************
		 * Button Profile
		 *************************************************/

		Button profileButton = (Button) findViewById(R.id.dashboard_btn_profile);
		profileButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// REMOVE COMMENT TAGS LATER mTracker.trackPageView("/profile");
				mActivityHelper.startActivity(SettingsActivity.class);
			}
		});

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// Check if we are authorized with facebook
		
		if (resultCode == RESULT_FIRST_USER) {
			//Log.i("NEED TO FINISH HERE!!!!!!!!!!", " ");
			finish();
		}
	}
	
	
	//Menu Items Inflating
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.home_menu_items, menu);
		super.onCreateOptionsMenu(menu);
		return true;
	}

	//What happens when menu item is selected
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return mActivityHelper.onOptionsItemSelected(item)
				|| super.onOptionsItemSelected(item);
	}
	
	//Survey voting function
	private void vote(int surveyNum) {
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		switch (surveyNum) {
		case 1:
			editor.putBoolean("surveyPlacesVoted", true);
			if (editor.commit()) {
				// surveyPlacesVoted = true;
			}
		case 0:
			editor.putBoolean("surveyToursVoted", true);
			if (editor.commit()) {
				surveyToursVoted = true;
			}
		}

	}

	//What happens when action bar item is selected
	@Override
	public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {
		switch (item.getItemId()) {
		case R.id.action_bar_info:

			final Dialog dialog = new Dialog(mContext);

			dialog.setContentView(R.layout.info);
			dialog.setTitle("Short info about us");
			dialog.show();
			break;

		}
		return super.onHandleActionBarItemClick(item, position);
	}

	//ActionBar initialization
	private void initActionBar() {
		addActionBarItem(getActionBar().newActionBarItem(
				NormalActionBarItem.class).setDrawable(
				R.drawable.gd_action_bar_info).setContentDescription(
				R.string.settings), R.id.action_bar_info);
	}

	/***************************************************
	 * DEFAULT ONSTOP & ONDESTROY &OnBackPressed - NO NEED TO SHOW LOGIN SCREEN AGAIN SO PUT
	 * "1" AS RESULT
	 ***************************************************/

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		setResult(RESULT_FIRST_USER);
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		setResult(RESULT_FIRST_USER);
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		Bundle bundle = new Bundle();
		// bundle.putString(FIELD_A, mA.getText().toString());

		Intent mIntent = new Intent();
		mIntent.putExtras(bundle);
		setResult(RESULT_FIRST_USER, mIntent);
		super.onBackPressed();
	}
}
