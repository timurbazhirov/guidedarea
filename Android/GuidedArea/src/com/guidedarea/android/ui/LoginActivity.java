/*
 * Class that represents the Login Activity Screen for when user first open the app
 * Has textviews to get user info and facebook button to register through it
 * @author Timur Bazhirov, Mar 26, 2012
 * Using Parse external library
 */

package com.guidedarea.android.ui;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.guidedarea.GuidedAreaApplication;
import com.guidedarea.R;

import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;
import com.parse.ParseFacebookUtils.Permissions;

public class LoginActivity extends Activity {

	// Declarations
	private final String FACEBOOK_APP_ID = "116451191790013";
	private final String PARSE_APP_ID = "bAMGYsME9tgqpa4uIeeT49FgsgMrUbl8pKbirrmK";
	private final String PARSE_CLIENT_KEY = "NKEsn6elLnj50BjHo8yFZXsRkcJaUs65zRVO6Rmv";
	private int FB_INT_FLAG = 13; // RequestCode Flag for the OnActivityResult
	private com.parse.facebook.Facebook mFacebook; // Parse library that contains all current facebook library data
	
	private SharedPreferences mPrefs;
	int requestCode;
	private boolean ActivityStarted = false;
	String TAG = "######### GUIDED AREA APPLICATION ######### LOGIN_ACTIVITY";
	
	// GoogleAnalytics
	private GoogleAnalyticsTracker mTracker;
	
	/***************************************************************************
	 * 
	 * 					          ONCREATE
	 * 
	 **************************************************************************/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		// Google Analytics
		mTracker = ((GuidedAreaApplication)	getApplication()).getTrackerInstance();
		mTracker.trackPageView("/area_explore");

		// PARSE initialization
		Parse.initialize(this, PARSE_APP_ID, PARSE_CLIENT_KEY);
		ParseFacebookUtils.initialize(FACEBOOK_APP_ID, true);
		mFacebook = ParseFacebookUtils.getFacebook();

		// FB initialization: get the facebook authorization status from shared preferences and update mFacebook

		mPrefs = getPreferences(MODE_PRIVATE);
		String access_token = mPrefs.getString("access_token", null);
		long expires = mPrefs.getLong("access_expires", 0);

		if (expires != 0) {
			mFacebook.setAccessExpires(expires);
		}

		if (access_token != null) {
			mFacebook.setAccessToken(access_token);
		}

		// Buttons initialization

		//final EditText Email = (EditText) findViewById(R.id.lgn_email);
		//final EditText Password = (EditText) findViewById(R.id.lgn_password);

		//Button registerScreen = (Button) findViewById(R.id.link_to_register);
		//Button loginBtn = (Button) findViewById(R.id.btnLogin);
		Button FbBtn = (Button) findViewById(R.id.ButtonFB);
		Button loginskipBtn = (Button) findViewById(R.id.btnskipLogin);


		/***************************************************************************
		 * Listening to register new account link
		 **************************************************************************/
/*
		registerScreen.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// Switching to Register screen
				Intent i = new Intent(getApplicationContext(),
						RegisterActivity.class);
				startActivity(i);
			}
		});
*/	
		/*************************************************************************** 
		 *       Listening to FB button 
		 **************************************************************************/

		FbBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!mFacebook.isSessionValid()) {
					
					Toast.makeText(LoginActivity.this, "Authorizing",
							Toast.LENGTH_SHORT).show();

					/**
					 * PARSE FB LOGIN PART
					 */

					ParseFacebookUtils.logIn(
							LoginActivity.this, FB_INT_FLAG,
							new LogInCallback() {
								@Override
								public void done(ParseUser user,
										ParseException err) {
									// TODO Auto-generated method stub
									if (user == null) {

										Log
												.d(TAG,
														"Uh oh. The user cancelled the Facebook login.");

									} else if (user.isNew()) {

										Log
												.d(TAG,
														"User signed up and logged in through Facebook!");
										// ParseLinkUser(user);
										//startedNextActivity = true; 
										if (!ActivityStarted) {
											myIntentStart();
										}

									} else {
										Log
												.d(TAG,
														"User logged in through Facebook!");
										//startedNextActivity = true;
										if (!ActivityStarted) {
											myIntentStart();
										}
									}

								}
							});
				}
			}
		});

		/*************************************************************************** 
		 *         Listening to login button 
		 **************************************************************************/
/*
		loginBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				String mEmail = Email.getText().toString().trim();
				String mPswrd = Password.getText().toString().trim();

				if (!mEmail.equals("") && !mPswrd.toString().equals("")) {

					ProgressDialog progress = new ProgressDialog(
							LoginActivity.this);
					progress.setMessage("Logging in...");

					new ProgressBarTask(progress, mEmail, mPswrd).execute();

				} else {
					Toast.makeText(LoginActivity.this,
							"Please fill in all fields", Toast.LENGTH_SHORT)
							.show();
				}

			}
		});
*/
		/**************************************************************************** 
		 *      Listening to login skip button 
		 **************************************************************************/
		loginskipBtn.setEnabled(false); // User shouldn't be able to see this button
		loginskipBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO: check creds before switching to Home
				// Switching to Home screen
				myIntentStart();

			}
		});

	}

	
	
	/***************************************************
	 * 
	 * 		DEFAULT ONSTART
	 * 
	 ***************************************************/

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onStart();

		//if (mFacebook.isSessionValid() && (!ActivityStarted)) {
			Log.i("ONRESUME IS SHOOOOOOOTING", " ");
			myIntentStart();
		//}
	}

	
	
	/***************************************************
	 * 
	 * 		DEFAULT OnActivityResult - NO NEED TO SHOW LOGIN SCREEN AGAIN
	 * 
	 ***************************************************/

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		// Check if we are authorized with facebook
		Log.i(TAG, " On ActivityResult " );
		
		if ((resultCode == RESULT_FIRST_USER) || (mFacebook.isSessionValid())) {
			Log.i(TAG, " On ActivityResult FINISHING!!! ");
			finish();
			// A hack introduced to not show the dialog screen twice in SightseeingMapActivity when screen is rotated once
			ActivityStarted = true;
			Log.e(TAG, " On ActivityResult DID NOT FINISH Properly! ");
		}
		
		if (!mFacebook.isSessionValid()) {
			ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
			Log.i(TAG, " Facebook authorization went well. OnActivityResult. " );
			//myIntentStart();
			}
		Log.i(TAG + " " + "requestCode, resultCode, data", String.valueOf(requestCode)
				+ " " + String.valueOf(resultCode) + " " + data);
		// Check if we're coming back from Dashboard and close the app

	}

	
	
	/***************************************************
	 * 
	 * 		INTENT STARTING FOR RESULT
	 * 
	 ***************************************************/
	public void myIntentStart() {

		Intent i = new Intent(getApplicationContext(), SightseeingMapActivity.class);
		ActivityStarted = true;
		startActivityForResult(i, requestCode);
	}

	
	
	/***************************************************
	 * 
	 * 		PROGRESS BAR IMPLEMENTATION - Not used currently
	 * 
	 ***************************************************/

	public class ProgressBarTask extends AsyncTask<Void, Void, Void> {
		private ProgressDialog progress;
		private String name;
		private String pswd;
		private boolean loggedin;
		private ParseUser newuser;

		public ProgressBarTask(ProgressDialog progress, String name, String pswd) {
			this.name = name;
			this.pswd = pswd;
			this.progress = progress;
		}

		public void onPreExecute() {
			progress.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Do the activity

			try {
				newuser = ParseUser.logIn(name, pswd);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			/*
			ParseUser.logInInBackground(name, pswd, new LogInCallback() {
				public void done(ParseUser user, ParseException e) {
					
					if (e == null && user != null) {
						// Hooray! The user is logged in.
						loggedin = true;
						Log.d("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!GuidedArea",
								"User login went right.");
						progress.dismiss();

						myIntentStart();

					} else if (user == null) {
						// Sign up didn't succeed. The username or password was
						// invalid.
						loggedin = false;
						Log.e("GuidedArea", "User login Error", e);
						Toast
								.makeText(LoginActivity.this,
										"Invalid username/password",
										Toast.LENGTH_SHORT).show();
					} else {
						// There was an error. Look at the ParseException to see
						// what happened.
						loggedin = false;
						Log.e("GuidedArea", "User login Error", e);
						Toast.makeText(LoginActivity.this,
								"Server Login Error", Toast.LENGTH_SHORT)
								.show();
					}
				}
			});
			*/			
			return null;
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

		public void onPostExecute(Void unused) {


			progress.dismiss();
			
			if (newuser!=null) {
				// Switching to Dashboard screen
				Log.d(TAG,
						"User login went right. ");
				myIntentStart();
			}
			else{
				Toast.makeText(LoginActivity.this,
						"Login Error: wrong username and/or password", Toast.LENGTH_SHORT)
						.show();
				Log.d(TAG,
				"User login went wrong. ");
			}

		}
	}



	@Override
	protected void onDestroy() {
		super.onDestroy();
		mTracker.stopSession();
	}
	

}