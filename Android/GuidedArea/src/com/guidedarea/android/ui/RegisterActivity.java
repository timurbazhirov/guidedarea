/*
 * Class that represents the Login Activity Screen for when user first open the app
 * Has textviews to get user info and facebook button to register through it
 * @author Timur Bazhirov, Mar 26, 2012
 * Using Parse external library
 */

package com.guidedarea.android.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;
import com.guidedarea.R;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class RegisterActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		final EditText Fullname = (EditText) findViewById(R.id.reg_fullname);
		final EditText Email = (EditText) findViewById(R.id.reg_email);
		final EditText Password = (EditText) findViewById(R.id.reg_password);
		Button Register = (Button) findViewById(R.id.btnRegister);

		TextView loginScreen = (TextView) findViewById(R.id.link_to_login);

		// Listening to Login Screen link
		loginScreen.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				// Closing registration screen
				// Switching to Login Screen/closing register screen
				finish();
			}
		});

		Register.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				String mName = Fullname.getText().toString().trim();
				String mEmail = Email.getText().toString().trim();
				String mPswrd = Password.getText().toString().trim();

				if (!mName.equals("") && !mEmail.equals("")
						&& !mPswrd.toString().equals("")) {

					if (isValidEmail(mEmail)) {

						ParseUser user = new ParseUser();
						user.setUsername(mEmail);
						user.setPassword(mPswrd);
						user.setEmail(mEmail);
						user.put("Fullname", mName);

						// other fields can be set just like with ParseObject
						// user.put("phone", "650-253-0000");

						ProgressDialog progress = new ProgressDialog(
								RegisterActivity.this);
						progress.setMessage("Registering...");

						new ProgressBarTask(progress, user).execute();
					} else {
						Toast.makeText(RegisterActivity.this,
								"Invalid Email Field", Toast.LENGTH_SHORT)
								.show();
					}

				} else {
					Toast.makeText(RegisterActivity.this,
							"Please fill in all fields", Toast.LENGTH_SHORT)
							.show();
				}

			}
		});
	}

	/***************************************************
	 * PROGRESS BAR IMPLEMENTATION
	 ***************************************************/

	public class ProgressBarTask extends AsyncTask<Void, Void, Void> {
		private ProgressDialog progress;
		private ParseUser user;

		public ProgressBarTask(ProgressDialog progress, ParseUser user) {
			this.progress = progress;
			this.user = user;
		}

		public void onPreExecute() {
			progress.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Do the activity

			user.signUpInBackground(new SignUpCallback() {

				@Override
				public void done(com.parse.ParseException e) {
					// TODO Auto-generated method stub
					if (e == null) {
						// Hooray! Let them use the app now.
						Log.d("GuidedArea", "User registration went right.");

					} else {
						// Sign up didn't succeed. Look at the ParseException
						// to figure out what went wrong
						Log.e("GuidedArea",
								"Uh oh. The user registration went wrong.", e);
					}

				}
			});
			return null;
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

		public void onPostExecute(Void unused) {

			// Switching to Dashboard screen
			Intent i = new Intent(getApplicationContext(), HomeActivity.class);
			startActivity(i);
			progress.dismiss();
		}
	}

	/*****************************************
	 * Email VALIDATOR
	 * 
	 * @param email
	 * @return
	 *****************************************/

	public final static boolean isValidEmail(CharSequence target) {
		try {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
					.matches();
		} catch (NullPointerException exception) {
			return false;
		}
	}

}
