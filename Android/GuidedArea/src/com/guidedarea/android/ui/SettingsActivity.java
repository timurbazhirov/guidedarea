package com.guidedarea.android.ui;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONObject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.guidedarea.GuidedAreaApplication;
import com.guidedarea.R;
import com.parse.ParseException;
import com.parse.SaveCallback;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;

public class SettingsActivity extends PreferenceActivity {
	private final String FACEBOOK_APP_ID = "116451191790013";
	private com.parse.facebook.Facebook mFacebook;
	//private Facebook facebook = new Facebook(FACEBOOK_APP_ID);
	//private AsyncFacebookRunner mAsyncRunner = new AsyncFacebookRunner(facebook);
	private SharedPreferences mPrefs;
	// GoogleAnalytics
	private GoogleAnalyticsTracker mTracker;

	/*******************************************************************************
	 * ON CREATE
	 *******************************************************************************/
		
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// visual settings + preferences		
		addPreferencesFromResource(R.xml.preferences);
		setTheme(R.style.PreferencesTheme);
		
		// Google analytics		
		mTracker = ((GuidedAreaApplication)	getApplication()).getTrackerInstance();
		mTracker.trackPageView("/settings");
		
		//Parse initialization		
		ParseFacebookUtils.initialize(FACEBOOK_APP_ID, true);
		mFacebook = ParseFacebookUtils.getFacebook();

		// Facebook preference indicator 		
		Preference fbConnect = findPreference("facebook");
		
    	 //Get existing access_token if any		
		mPrefs = getPreferences(MODE_PRIVATE);
		String access_token = mPrefs.getString("access_token", null);
		long expires = mPrefs.getLong("access_expires", 0);

		if (expires != 0) {
			mFacebook.setAccessExpires(expires);
		} else {
			fbConnect.setSummary("Your session expired. Please, reconnect.");
		}

		if (access_token != null) {
			mFacebook.setAccessToken(access_token);
		} else {
			fbConnect.setSummary("You're not connected to facebook.");
		}

		if (mFacebook.isSessionValid()) {
			fbConnect.setSummary("Connected to facebook. Tap to logout.");
		}

		fbConnect.setOnPreferenceClickListener(new OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
											
				final ParseUser user = ParseUser.getCurrentUser();				
				Toast.makeText(SettingsActivity.this, "Authorizing" + user, Toast.LENGTH_SHORT);
				 //* Only call authorize if the access_token has expired.
				
				if (!mFacebook.isSessionValid()) {
					
					Toast.makeText(SettingsActivity.this, "Authorizing", Toast.LENGTH_SHORT);
					
					if (!ParseFacebookUtils.isLinked(user)) {
					    ParseFacebookUtils.link(user, SettingsActivity.this, new SaveCallback() {
					        @Override
					        public void done(ParseException ex) {
					            if (ParseFacebookUtils.isLinked(user)) {
					                Log.d("MyApp", "Woohoo, user logged in with Facebook!");
					                UpdatePreference("facebook", "Logged in through facebook!.");
					            }
					        }
					    });
					}
				} else {

					Toast.makeText(SettingsActivity.this, "Logging out", Toast.LENGTH_SHORT);
					Log.i("FACEBOOK_LOGOUT", "Facebook Dialog");

					ParseFacebookUtils.unlinkInBackground(user, new SaveCallback() {

						@Override
						public void done(ParseException ex) {
							// TODO Auto-generated method stub
							if (ex == null) {
					            Log.d("MyApp", "The user is no longer associated with their Facebook account.");
					        }
						}
					});

					UpdatePreference("facebook", "Logged out. Tap to log in.");

				}

				return false;
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		super.onActivityResult(requestCode, resultCode, data);
		ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
	}

	public void UpdatePreference(String PreferenceName, String Update) {

		Preference mConnect = findPreference(PreferenceName);
		mConnect.setSummary(Update);

	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		mTracker.stopSession();
	}
}

