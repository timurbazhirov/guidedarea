/*
 * Class that represents the Map Activity Screen for the Guide regime.
 * Has listeners to track location and open info pop-ups or start audio
 * Main Class of the app
 * @author Timur Bazhirov, Sep 5, 2012
 * Using GreenDroid and Parse.com external libraries
 */

package com.guidedarea.android.ui;


import org.apache.commons.net.telnet.TelnetClient;
import greendroid.app.GDMapActivity;
import greendroid.widget.ActionBar;
import greendroid.widget.ActionBarItem;
import greendroid.widget.LoaderActionBarItem;
import greendroid.widget.NormalActionBarItem;

import java.io.IOException;
import java.io.PrintStream;
import java.net.SocketException;
import java.util.List;

import org.json.JSONException;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.Gravity;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomButtonsController.OnZoomListener;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.guidedarea.GuidedAreaApplication;
import com.guidedarea.R;
import com.guidedarea.android.adapters.ParsetoGA;
import com.guidedarea.android.listeners.AudioGuideLocationListener;
import com.guidedarea.android.listeners.GaLocationListener;
import com.guidedarea.android.listeners.GpsStatusListener;
import com.guidedarea.android.listeners.SilentGuideLocationListener;
import com.guidedarea.android.listeners.VisualGuideLocationListener;
import com.guidedarea.android.maps.GuidedMapView;
import com.guidedarea.android.maps.GuidedObjectItemizedOverlay;
import com.guidedarea.android.maps.GuidedObjectOverlayItem;
import com.guidedarea.android.maps.UserLocationOverlay;
import com.guidedarea.android.maps.ZoomListener;
import com.guidedarea.android.models.GArea;
import com.guidedarea.android.utils.AppStatus;
import com.guidedarea.android.utils.LocQualityChecker;
import com.guidedarea.android.utils.MapActivityHelper;
import com.guidedarea.android.utils.NuanceTtsHelper;
import com.guidedarea.android.utils.TypeConverter;
import com.guidedarea.android.utils.ZoomHelper;
import com.guidedarea.android.utils.ZoomLevel;
import com.nuance.nmdp.speechkit.SpeechError;
import com.nuance.nmdp.speechkit.SpeechKit;
import com.nuance.nmdp.speechkit.Vocalizer;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class SightseeingMapActivity extends GDMapActivity implements 
		ZoomListener, SensorEventListener {

	// Fake GPS related stuff
	private static final boolean EMULATOR = true;
	private static float LOC_STEP = 0.0001f;	//amount GPS simulator increases every keypress	
	private double lon = -122.258754;
	private double lat = 37.875836;
	private boolean dragging = false;
	public static GeoPoint current = new GeoPoint(37875836,-122258754);
	public static GeoPoint Cal = new GeoPoint(37872145,-122259518);
	private TelnetClient telnet = new TelnetClient();
	public static GeoPoint LastDownload = new GeoPoint(37875836,-122258754);
	
	// Default level to open the map with
	public static final int ZOOM_DEFAULT_LEVEL = 14;
	// Number of POIs to be shown to user at onsetuserlocation
	public static final int NUMBER_OF_POI = 12;

	// Flags for the initOverlays function
	private static boolean UserLocationDrawn;
	private static boolean initFlag = true;
	private static boolean clearMapFlag = false;

	// Overlays related variables
	private MapActivityHelper mMapActivityHelper;
	private GuidedMapView _mMapView;
	private GuidedObjectItemizedOverlay mGuidedObjectItemizedOverlay;
	private List<Overlay> mOverlays;

	// Log output TAG
	private final String TAG = "######### GUIDED AREA APPLICATION ######### SIGHTSEEING_MAP_ACTIVITY";

	// Location Handling
	private LocationManager mLocManager;
	protected GaLocationListener mLocListener;
	private GpsStatusListener gpsStatusListener = new GpsStatusListener();
	private UserLocationOverlay mUserLocOverlay;

	// Location Providers
	private String mGpsProvider = LocationManager.GPS_PROVIDER;
	private String mNetworkProvider = LocationManager.NETWORK_PROVIDER;
	
	// Power and screen lock management
	private PowerManager mPowerManager;
	PowerManager.WakeLock WakeLock;

	// Fields initialization in initPreferences
	private long mGpsUpdateInterval; // In milliseconds
	private float mGpsMoveInterval; // In meters
	private int mTtsEngine; // 0 - nuance, 1 - default
	private int mGuideType; // 0 - visual, 1 - audio
	private int mGuideAuto; // 0 - off, 1 - on
	private int mGuideLaunchDistance; // in meters
	private boolean isFirstRun;

	private RelativeLayout rlObjectInfo;// relative layout object handling
	private static Button stopTTS; // Button used to Stop Text-To-Speech

	private String ParseId;
	private GArea gArea_write;
	
	// Action Bar related stuff
	private final Handler mHandler = new Handler(); // For action bar refresh
	private LoaderActionBarItem loaderItem;
													
	// Zoom level handling
	private ZoomLevel mZoomLevel = ZoomLevel.AREA;
	private ZoomLevel mOldZoomLevel = ZoomLevel.AREA;

	// Needed to clear up the map
	GuidedObjectItemizedOverlay OldGuidedObjectItemizedOverlay;

	// Init Vocalizer + related
	private Vocalizer _vocalizer;
	Vocalizer.Listener _vocalizerListener;
	private boolean VocalizerSpeaking = false;
	private Object _lastTtsContext = null;

	RelativeLayout.OnClickListener _Listener;
	Button.OnClickListener _Listener1;
	
	private class SavedState {
		GaLocationListener mLocListener;
		LocationManager mLocManager;
		RelativeLayout rlObjectInfo;
		RelativeLayout.OnClickListener Listener;
		Vocalizer vocalizer;
		Vocalizer.Listener vocalizerListener;
		Object context;
		List<Overlay> mOverlays;
		GuidedMapView mMapView;
		GuidedObjectItemizedOverlay mGuidedObjectItemizedOverlay;
		UserLocationOverlay mUserLocOverlay;
		MapActivityHelper mMapActivityHelper;
		boolean dontShowDialog;
		Dialog dialog;
	}

	SavedState savedState;
	
	// Distance to POI
	float _distance = 0; 
	
	
	// Rotating Arrow related variables
	private float[] mag = new float[] {0,0,0};
	private float[] acc = new float[] {0,0,0};
	public Drawable pointer;
	public int pointerWidth, pointerHeight;
	public Bitmap starplace;
	private float phase = 0f;
	protected SensorManager mSensorManager;
	protected Sensor mAccelerometer;
	protected Sensor mCompass;
	private long last_updated_time;
	
	// AlertDialog that only shown first time the app starts
	public static final String PREFS_NAME = "GA_PREFS";
	public static final int DISTANCE_TO_CAL = 1000; // in meters to the center of campus 
	private boolean dontShowDialog = false;
	private Dialog dialog;
	
	// GoogleAnalytics
	private GoogleAnalyticsTracker mTracker;

	
	
	
	
	/**********************************************
	 * 
	 * 				ON CREATE FUNCTION
	 * 
	 **********************************************/

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	
		// Layout initialization
		setActionBarContentView(R.layout.activity_sightseeing_map);

		// Create MapActivityHelper
		mMapActivityHelper = MapActivityHelper.createInstance(this);

		// Initialize with value for future use for overlays clearing
		OldGuidedObjectItemizedOverlay = new GuidedObjectItemizedOverlay(
				getResources().getDrawable(R.drawable.ga_map_pin), this);
		
		// Rotating Arrow related stuff:		
		pointer = this.getResources().getDrawable(R.drawable.pointer);
		pointerWidth = pointer.getIntrinsicWidth();
		pointerHeight = pointer.getIntrinsicHeight();
		starplace = BitmapFactory.decodeResource(getResources(), R.drawable.marker_point_focused);
		
		// Power Manager for screen lock
		mPowerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
		WakeLock = mPowerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "SightSeeing");
		
		// Dialog dontShowDialog flag related initialization
		final SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		dontShowDialog = settings.getBoolean("dontShowDialog", false);

		// Google Analytics
		mTracker = ((GuidedAreaApplication) getApplication()).getTrackerInstance();
		mTracker.trackPageView("/sightseeing");

		// Initialize Preferences + Action bar
		initPreferences();
		initActionBar();
		
		// Init Map
		initMapView();

		// create Vocalizer related objects
		_vocalizerListener = new Vocalizer.Listener() {
			@Override
			public void onSpeakingBegin(Vocalizer vocalizer, String text,
					Object context) {
				VocalizerSpeaking = true;
				
				// Hide StopTTS button
				HideStop();
				// AreaExploreActivity.this.getActionBar().getItem(0).setDrawable(R.drawable.gd_action_bar_gallery);
			}

			@Override
			public void onSpeakingDone(Vocalizer vocalizer, String text,
					SpeechError error, Object context) {

				// Use the context to determine if this was the final TTS phrase

				// AreaExploreActivity.this.getActionBar().getItem(0).setDrawable(R.drawable.gd_play_icon);

				VocalizerSpeaking = false;
				
				// Show StopTTS button
				ShowStop();
				
				if (context != _lastTtsContext) {
					// TODO: change button to PLAY
				} else {

				}
			}
		};		
		
		// Check for saved state

		SavedState savedState = (SavedState) getLastNonConfigurationInstance();
		
		

		if (savedState == null) {

			// Prepare Maps
			UserLocationDrawn = false;

			// Initialize Map + Put Overlays
			initFlag = true;
			initOverlays("0", "1");

			// Initiate Nuance
			InitNuance();
			
			
			// Guide Type based Location Listener Initialization
			initListener();

			// GPS/GSM Service and current coordinates Initialization

			mLocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			mLocManager.requestLocationUpdates(mGpsProvider,
					mGpsUpdateInterval, mGpsMoveInterval, mLocListener);
			mLocManager.addGpsStatusListener(gpsStatusListener);
			mMapActivityHelper.centerOnLocation(_mMapView, mLocManager);
			
		}

		else {
			RetrieveState(savedState);
			
			setResult(RESULT_FIRST_USER);
			
			if (_vocalizer == null) {
				InitNuance();			}
			
			
			// dismissing the dialog in case it was shown before, then it might be recreated later in this function
			if (dialog != null && dialog.isShowing()) {
			      dialog.dismiss();
			} 

			// Need to update the listener, since the old listener is pointing
			// at the old TtsView activity instance.
			_vocalizer.setListener(_vocalizerListener);
			
			// Init Overlays from previous ones
			mOverlays = _mMapView.getOverlays();
			Log.d(TAG + " mMapView size",String.valueOf(_mMapView.getOverlays().size()));
			// Need to update the activity context in GuidedObjectItemizedOverlay
			
			// Added if below because it was causing forseclosure on rotation when overlays were still being downloaded
			if (mGuidedObjectItemizedOverlay != null) {
				mGuidedObjectItemizedOverlay.setContext(this);			
				mOverlays.add(mGuidedObjectItemizedOverlay);
			}
			else{
				Toast.makeText(this,"Screen was rotated during download! Please tap update icon to show markers.",Toast.LENGTH_LONG).show();
			}
			
			OverlayArrow arrowoverlay = new OverlayArrow();
			mOverlays.add(arrowoverlay);
			_mMapView.invalidate();
			
		}


		// Relative layout OnClick listener
		
		rlObjectInfo = (RelativeLayout) findViewById(R.id.rlObjectInfo);
		
		_Listener = new RelativeLayout.OnClickListener() {

			public void onClick(View v) {

				// put necessary parameters in and
				Intent intent;
				intent = new Intent(getApplicationContext(),
						AreaExploreActivity.class);

				// Check if parameters are initialized already
				if (ParseId != null) {
					// parameters are initialized - starting activity
					intent.putExtra("Distance", TypeConverter.formatDistance(_distance));
					intent.putExtra("Description", gArea_write.getDescription());
					intent.putExtra("Name", gArea_write.getName());
					intent.putExtra("PhotoLink", gArea_write.getPhotoLink());

					Log.i(TAG, "GArea = " + gArea_write.getName());
					Log.d(TAG, "Distance = " + _distance);
					startActivity(intent);
				} else {
					// parameters not initialized yet, waiting 200ms
					mHandler.postDelayed(new Runnable() {
						public void run() {

							Log.i(TAG,
									"Parameters were not initialized: ParseId");
							Intent intent;
							intent = new Intent(getApplicationContext(),
									AreaExploreActivity.class);

							intent.putExtra("Distance", TypeConverter.formatDistance(_distance));
							intent.putExtra("Description",
									gArea_write.getDescription());
							intent.putExtra("Name", gArea_write.getName());
							intent.putExtra("PhotoLink",
									gArea_write.getPhotoLink());
							startActivity(intent);

						}
					}, 200);
				}

			}
		};

		rlObjectInfo.setOnClickListener(_Listener);
		
		
		// Stop Text-to-speech button handling
		
		stopTTS = (Button) findViewById(R.id.stopTTS);
		
		_Listener1 = new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				// Hide StopTTS button
				HideStop();

				if (_vocalizer != null)	{
					_vocalizer.cancel();
				}
				
				mLocListener.Stop();
				/*
				if (VocalizerSpeaking) {
					_vocalizer.cancel();
					VocalizerSpeaking = false;
				}
				*/
			}
		};
		
		stopTTS.setOnClickListener(_Listener1);

		
		// Rotating Arrow related stuff
		mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
		Log.d(TAG,"OnCreate: START listening to the SENSOR_MAG_FIELD");
		
		// Get the screen Lock Acquired
		//PowerManager.WakeLock wl = powerManager.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.SCREEN_DIM_WAKE_LOCK, SightseeingMapActivity.class.getSimpleName());
		WakeLock.acquire();
		Log.d(TAG,"OnCreate: Wake Lock acquired");		
		
		
		// Alert Dialog - shown to user when the app starts depending on how close he/she is to campus and dontShow flag
		
		dialog = new Dialog(this);
		dialog.setContentView(R.layout.intro);
		dialog.setTitle("Welcome!");
		
		CheckBox ChckBox = (CheckBox) dialog.findViewById(R.id.checkbox_intro);
		TextView TxtView = (TextView) dialog.findViewById(R.id.textView_intro);
		ChckBox.setOnClickListener(new OnClickListener() {
			 
			  @Override
			  public void onClick(View v) {

				if (((CheckBox) v).isChecked()) {
					SharedPreferences.Editor editor = settings.edit();
					editor.putBoolean("dontShowDialog", true);
					if (editor.commit()) { 
						dontShowDialog = true;
					}
				}
		 
			  }
			});
		 
		
		Button dialogButton = (Button) dialog.findViewById(R.id.button_intro_OK);
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {				
				// Don't show the intro dialog this time the app starts and on rotation to not to annoy user
				dontShowDialog = true;
				dialog.dismiss();
			}
		});
		
		if (!dontShowDialog) { 
				Log.e(TAG, "dontShowDialog was false");
				if (LocQualCheck().distanceTo(TypeConverter.geoPointToLocation(Cal)) > DISTANCE_TO_CAL) {
					dialog.show(); //- caused leaked window
				}
				else {
					TxtView.setText(R.string.app_intro_on_campus);
					dialog.show(); //- caused leaked window
				}
		}
	}
	

	
	
	
	/**********************************************
	 * 
	 * Stop Text-To-Speech button-related functions - separated so that they will be available for use from other activities
	 * 
	 **********************************************/	
	public static void ShowStop(){
		stopTTS.setVisibility(View.VISIBLE);
	}
	
	public static void HideStop(){
		stopTTS.setVisibility(View.INVISIBLE);
	}


	
	
	
	
	/**********************************************
	 * 
	 * INIT Listener FUNCTION - Get Listener connected to the map
	 * 
	 **********************************************/	
	protected void initListener(){
		
		if (mGuideAuto == 1){		
			
			switch (mGuideType) {
			case 0:
				mLocListener = new VisualGuideLocationListener(this,
						mGuideLaunchDistance,this);
				break;
			case 1:
				mLocListener = new AudioGuideLocationListener(this, mTtsEngine,
						mGuideLaunchDistance, this, _vocalizer); // NOTABENE!
				break;
			default:
				mLocListener = new AudioGuideLocationListener(this, mTtsEngine,
						mGuideLaunchDistance, this, _vocalizer);
				break;
			}
		}
		else {
			mLocListener = new SilentGuideLocationListener(this,
					mGuideLaunchDistance,this);
		}
	}

	
	
	/**********************************************	 * 
	 * IS ROUTE DISPLAYED FUNCTION - Default, needs to be overrided	 * 
	 **********************************************/

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	
	
	
	
	/**********************************************
	 * 
	 * INIT PREFERENCES FUNCTION - Get Preferences about map view
	 * 
	 **********************************************/

	private void initPreferences() {
		Log.i(TAG, "initPreferences() called");
		SharedPreferences shPrefs = mMapActivityHelper.getPreferences();
		// types may need to be adjusted
		try {
			mGpsUpdateInterval = Long.valueOf(shPrefs.getString(
					"gpsUpdateInterval", "5000"));
			mGpsMoveInterval = Float.valueOf(shPrefs.getString(
					"gpsUpdateDistance", "5"));
			mTtsEngine = Integer.valueOf(shPrefs.getString("ttsEngine", "1"));
			mGuideType = Integer.valueOf(shPrefs.getString("guideType", "1"));
			mGuideAuto = Integer.valueOf(shPrefs.getString("guideAuto", "1"));			
			mGuideLaunchDistance = Integer.valueOf(shPrefs.getString(
					"guideLaunchDistance", "30"));
		} catch (NumberFormatException nfe) {
			System.out.println("Shared preferences. Could not parse " + nfe);
		}
	}

	
	
	
	
	/**********************************************
	 * 
	 * INIT MAP FUNCTION - Initialize map view
	 * 
	 **********************************************/

	protected void initMapView() {
		Log.i(TAG, "initMapView() called");
			_mMapView = (GuidedMapView) findViewById(R.id.map_view_sightseeing);
			mMapActivityHelper.initMapView(_mMapView);
			_mMapView.setZoomListener(this);
			
			//The following line should only work on the very first attempt to start the app
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			isFirstRun = settings.getBoolean("isFirstRun", true);
			if (isFirstRun == true ){
				SharedPreferences.Editor editor = settings.edit();
				editor.putBoolean("isFirstRun", false);
				if (editor.commit()) { 
					isFirstRun = false;
				}
				_mMapView.setZoomLevel(ZOOM_DEFAULT_LEVEL);	
			}
			

			mUserLocOverlay = new UserLocationOverlay(this, _mMapView);
			mOverlays = _mMapView.getOverlays();
			mOverlays.add(mUserLocOverlay);

			Log.i(TAG, "initMapView() before useroverlay");
			mUserLocOverlay.enableMyLocation();
			UserLocationDrawn = true;
			//mUserLocOverlay.enableCompass();			
			Log.i(TAG, "initMapView() past user overlay");


	}

	
	
	
	
	/**********************************************
	 * ****************************************************
	 * INIT OVERLAYS FUNCTION - PUT OVERLAYS ON THE MAP DEPENDING ON THEIR TYPE
	 * ****************************************************
	 **********************************************/
	

	protected void initOverlays(String CenterFlag, String DialogFlag) {

		Log.i(TAG, "initOverlays() called");

		mOverlays = _mMapView.getOverlays();

		if (!mOldZoomLevel.equals(mZoomLevel) || initFlag == true
				|| clearMapFlag == true) {

			// get default zoom level
			if (initFlag == true) {
				int i = _mMapView.getZoomLevel();
				mZoomLevel = ZoomHelper.DetectZoom(i);
				initFlag = false;
			}
			
			// Progress Bar handling
			ProgressDialog progress = new ProgressDialog(this);
			progress.setMessage("Loading...");
			
			if (!AppStatus.getInstance(this).isOnline(this)){
				progress.setMessage("Looks like you are not online...");
				progress.show();
				Toast.makeText(this,"You are not online!!!!",Toast.LENGTH_LONG).show();
				return;
			}

			switch (mZoomLevel) {
			case COUNTRY:
				new GetAreasTask(progress, DialogFlag).execute("Country", CenterFlag);
				break;
			case CITY:
				new GetAreasTask(progress, DialogFlag).execute("City", CenterFlag);
				break;
			case AREA:
				new GetAreasTask(progress, DialogFlag).execute("Area", CenterFlag);
				break;
			case AREAOBJ:
				new GetAreasTask(progress, DialogFlag).execute("Areaobject", CenterFlag);
				break;
			default:
				new GetAreasTask(progress, DialogFlag).execute("Areaobject", CenterFlag);
			}
			mOldZoomLevel = mZoomLevel;
		}

		// TODO: ADD DRAWN OVERLAYS CACHE INSTEAD OF DRAWING ALL OF THEM
		// AGAIN!!!
		/*
		 * mUserLocOverlay = new UserLocationOverlay(this, mMapView);
		 * mOverlays.add(mUserLocOverlay);
		 * 
		 * if (!UserLocationDrawn) { mUserLocOverlay.enableMyLocation();
		 * UserLocationDrawn = true; mUserLocOverlay.enableCompass(); }
		 */
	}
	
	
	
	/**********************************************
	 * 
	 * 		CLEAR THE MAP
	 * 
	 **********************************************/

	protected void clearMap(){
		
	mGuidedObjectItemizedOverlay = new GuidedObjectItemizedOverlay(
			getResources().getDrawable(R.drawable.ga_map_pin), this);
	
	// clear marker overlays
	mOverlays.clear();
	mOverlays.add(mUserLocOverlay);
	// Rotating Arrow related stuff
	OverlayArrow arrowoverlay = new OverlayArrow();
	mOverlays.add(arrowoverlay);
	//_mMapView.invalidate();
	//mOverlays.remove(OldGuidedObjectItemizedOverlay);

	_mMapView.invalidate();
	clearMapFlag = false;
	}

	
	
	
	
	
	/*****************************************************
	 *****************************************************
	 * GET OVERLAYS DEPENDING ON THEIR TYPE WITH PARSE
	 *****************************************************
	 *****************************************************/
	

	private class GetAreasTask extends
			AsyncTask<String, Void, List<ParseObject>> {

		private ProgressDialog progress;
		private String zoomlevel;
		private String dialogflag;

		// Builder

		public GetAreasTask(ProgressDialog progress, String DialogFlag) {
			this.progress = progress;
			this.dialogflag = DialogFlag;
		}

		// Show progress bar

		public void onPreExecute() {	
			//progress.show();
			StartLoader();
		}

		@Override
		protected List<ParseObject> doInBackground(String... params) {

			Location loc = null; // init loc
			ParseGeoPoint UserLocation;

			// get current coordinates either GPS or Network

			loc = LocQualCheck();

			LastDownload = TypeConverter.locationToGeoPoint(loc);
			// Set the location to draw overlays from to be either center of the
			// map - 1 or user - 0

			switch (Integer.valueOf(params[1])) {
			case 0:
				break;
			case 1:
				loc.setLatitude((_mMapView.getMapCenter().getLatitudeE6()) / 1E6);
				loc.setLongitude((_mMapView.getMapCenter().getLongitudeE6()) / 1E6);
				break;
			default:
			}

			// Get Parse Objects

			if (loc != null) {
				UserLocation = new ParseGeoPoint(loc.getLatitude(),	loc.getLongitude());
			} else {
				Log.e(TAG + "doInBackground: ERROR! ", "Loc " + loc);
				return null;
			}

			ParseQuery query = new ParseQuery(params[0]);
			query.whereNear("location", UserLocation);
			query.setLimit(NUMBER_OF_POI);
			zoomlevel = params[0];

			List<ParseObject> nearPlaces = null;

			try {
				Log.i(TAG, "ParseQuery Shooting" + query);
				nearPlaces = query.find();
			} catch (ParseException e) {
				e.printStackTrace();
				Log.e(TAG + "doInBackground: Parse Exception!!! ", "Loc " + loc);
			}

			// ParseNearObjects = nearPlaces;
			return nearPlaces;
		}

		@Override
		protected void onPostExecute(List<ParseObject> result) {

			// create GAreas and overlays from PaseObjects
			
			clearMap();

			for (ParseObject a : result) {

				Log.i("onPostExecute", "ParseObject " + a);
//if no Internet connection show error screen
				GArea b = null;

				try {
					b = ParsetoGA.getArea(a);
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				Log.i(TAG + "onPostExecute", "GuidedObject " + b.getGeoPoint());

				mGuidedObjectItemizedOverlay
						.addOverlay(new GuidedObjectOverlayItem(b));

				if (zoomlevel=="Areaobject") {
					GuidedAreaApplication.addAreaObject(b); // TODO ADD OnDestroy!!!
				}
			}

			// add overlays to the map

			if (mGuidedObjectItemizedOverlay.size() > 0) {
				mOverlays.add(mGuidedObjectItemizedOverlay);
			}

			OldGuidedObjectItemizedOverlay = mGuidedObjectItemizedOverlay;

			// Progress Bar Dismissal
			//progress.dismiss();
			
			// Stopping the loader
			HideLoader();
		}
	}

	
	/**
	 * Method that is called from SilentGuideLocationListener to change color of the nearby overlay
	 * @param item
	 */
	
	 public void SetOverlayFocused(GArea item) {
		 	Log.d(TAG,"in SetOverlayFocused. GArea item =" + item);
		 	int position = mGuidedObjectItemizedOverlay.getOverlayItem(item);
		 	GuidedObjectOverlayItem overlay_item = mGuidedObjectItemizedOverlay.getItem(position);
			mGuidedObjectItemizedOverlay.setFocus(overlay_item);
	 }
	

	 
	 
	 
	/******************************************************
	 * ****************************************************
	 *      NATIVE METODS
	 * ****************************************************
	 ******************************************************/

	@Override
	protected void onStop() {
		setResult(RESULT_FIRST_USER);
		super.onStop();
		mUserLocOverlay.disableMyLocation();
		mLocManager.removeUpdates(mLocListener);
		mLocManager.removeGpsStatusListener(gpsStatusListener);
	}

	@Override
	protected void onPause() {
		// Rotating Arrow related
		mSensorManager.unregisterListener(this);
		Log.d(TAG,"OnPause: STOP listening to the SENSOR_MAG_FIELD");
		
		WakeLock.release();
		Log.d(TAG,"OnPause: Wake Lock released");
		
		super.onPause();
		mUserLocOverlay.disableMyLocation();
		Log.d(TAG,"OnPause: user location disabled");
		
		mLocManager.removeUpdates(mLocListener);
		mLocManager.removeGpsStatusListener(gpsStatusListener);
		//mLocListener.onPause();
		Log.d(TAG,"OnPause: STOP listening to the SENSOR_LOCATION. removeUpdates, removeGpsStatusListener");
		
		// Adding dialog.dismiss(); so the "Window leaked message don't appear here"
		if(dialog != null && dialog.isShowing()) {
		      dialog.dismiss();
		}
	}

	@Override
	protected void onDestroy() {
		setResult(RESULT_FIRST_USER);
		super.onDestroy();
		
		mLocManager.removeUpdates(mLocListener);
		mLocListener.onDestroy();
		mLocManager.removeGpsStatusListener(gpsStatusListener);
		Log.d(TAG,"OnDestroy: STOP listening to the SENSOR_LOCATION. removeUpdates, removeGpsStatusListener");
		
		mUserLocOverlay.disableMyLocation();
		Log.d(TAG,"OnDestroy: user location disabled");
		
		// remove magnetic field updates Rotating Arrow
		mSensorManager.unregisterListener(this);
		Log.d(TAG,"OnDestroy: STOP listening to the SENSOR_MAG_FIELD");
		
		WakeLock.release();
		Log.d(TAG,"OnDestroy: Wake Lock released");

		
		SpeechKit SpeechKit;
		SpeechKit = NuanceTtsHelper.getSpeechKit(this);
		
		if (SpeechKit != null) {
			if (_vocalizer != null) {
				
				try{
					_vocalizer.cancel();
				}
				catch (IllegalStateException e) {
					
				}

				// was giving force closure when combined with onBackPressed
				
				_vocalizer = null;
			}

			mLocListener.onDestroy();		
			
		}
		
		if(dialog != null && dialog.isShowing()) {
		      dialog.dismiss();
		}
		
		mTracker.stopSession();
	}

	@Override
	protected void onResume() {
		
		// Rotating Arrow related
		//mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_UI);
		//mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_NORMAL);
		mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_NORMAL);	
		Log.d(TAG,"On Resume: START listening to the SENSOR_MAG_FIELD");
		
		// WakeLock Acquisition
		WakeLock.acquire();
		Log.d(TAG,"OnResume: Wake Lock acquired");
		
		// Updating shared prefs and related things
		int oldGuideType = mGuideType;
		int oldTtsEngine = mTtsEngine;
		int oldGuideLaunchDistance = mGuideLaunchDistance;
		int oldGuideAuto = mGuideAuto;
		
		initPreferences();
		
		if ((mGuideAuto != oldGuideAuto) || (mGuideType != oldGuideType) || (mTtsEngine != oldTtsEngine) || (mGuideLaunchDistance != oldGuideLaunchDistance)) {
			initListener();
		}

		mLocManager.requestLocationUpdates(mGpsProvider,
				mGpsUpdateInterval, mGpsMoveInterval, mLocListener);
		
		super.onResume();
		// initOverlays("0");
     	 mUserLocOverlay.enableMyLocation();
		 mLocManager.requestLocationUpdates(mGpsProvider, mGpsUpdateInterval,
				mGpsMoveInterval, mLocListener);
	   	mLocManager.addGpsStatusListener(gpsStatusListener);
	}
	
	

	/*********************************************************************
	 * HANDLING ACTION BAR
	 * ****************************************************
	 * @see greendroid.app.GDMapActivity#onHandleActionBarItemClick(greendroid.widget.ActionBarItem,int)
	 * ****************************************************
	 *********************************************************************/	

	@Override
	public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {

		LocationManager myLocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		switch (item.getItemId()) {

		case R.id.action_bar_info:

			mMapActivityHelper.centerOnLocation(_mMapView, myLocManager);

			mHandler.postDelayed(new Runnable() {
				public void run() {

					// clearMapFlag = true; REMOVE_COMMENT_MARKERS_LATER!!!
					// initOverlays("0");

				}
			}, 1000);

			break;

		case R.id.action_bar_refresh:

			// mMapView.getOverlays().clear();

			clearMapFlag = true;
			initOverlays("0", "1");
			/*
			 * final LoaderActionBarItem loaderItem = (LoaderActionBarItem)
			 * item; mHandler.postDelayed(new Runnable() { public void run() {
			 * 
			 * 
			 * loaderItem.setLoading(false); } }, 2500); break;
			 */
		default:
			return super.onHandleActionBarItemClick(item, position);

		}
		return true;
	}
	
	
	public void HideLoader() {
		
		// A hacky way to remove the action bar item. #0 is the leftmost and #1 is the next - one we need to hide
		SightseeingMapActivity.this.getActionBar().removeItem(1);
		addActionBarItem(
				SightseeingMapActivity.this.getActionBar().newActionBarItem(NormalActionBarItem.class)
						.setDrawable(R.drawable.gd_action_bar_refresh)
						.setContentDescription(R.string.settings),
				R.id.action_bar_refresh);
	}
	
	
	public void StartLoader() {
		
		// A hacky way to remove the action bar item. #0 is the leftmost and #1 is the next - one we need to hide
		SightseeingMapActivity.this.getActionBar().removeItem(1);
	
		addActionBarItem(
				this.getActionBar()
						.newActionBarItem(LoaderActionBarItem.class)
						.setDrawable(R.drawable.gd_play_icon)
						.setContentDescription(R.string.settings),
				R.id.action_bar_info);

		loaderItem = (LoaderActionBarItem) this.getActionBar().getItem(1);
		loaderItem.setLoading(true);
	}
	
	
	
	
	
	/*****************************************************
	 * 
	 *   UPDATING OVERLAYS RELATED METHOD
	 *   
	 *****************************************************/

	public void UpdateOverlays() {
		
		LocationManager myLocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		
		mMapActivityHelper.centerOnLocation(_mMapView, myLocManager);
		clearMapFlag = true;
		initOverlays("0","1");
	}

	private void initActionBar() {

		this.getActionBar().setType(ActionBar.Type.Empty);
		this.getActionBar().setGravity(0x11);

		addActionBarItem(
				getActionBar().newActionBarItem(NormalActionBarItem.class)
						.setDrawable(R.drawable.gd_action_bar_locate_myself)
						.setContentDescription(R.string.settings),
				R.id.action_bar_info);

		addActionBarItem(
				getActionBar().newActionBarItem(NormalActionBarItem.class)
						.setDrawable(R.drawable.gd_action_bar_refresh),
				R.id.action_bar_refresh);

		this.getActionBar().setTitle("");
		TextView tv = (TextView) this.findViewById(R.id.gd_action_bar_title);
		tv.setCompoundDrawablesWithIntrinsicBounds(this.getResources()
				.getDrawable(R.drawable.greendroid_application_logo), null,
				null, null);
	}
	
	

	/**********************************
	 * 
	 *  WHEN AREA TAPPED - INFO SHOWING
	 *  
	 **********************************/

	public void showObjInfo(GArea gArea) {

		// mTracker.trackPageView("/area_tap/" + gArea.getName().replaceAll(" ",
		// ""));

		rlObjectInfo.setVisibility(View.VISIBLE);
		rlObjectInfo.bringToFront();

		TextView tvName = (TextView) findViewById(R.id.tvName);
		TextView tvDist = (TextView) findViewById(R.id.tvDist);

		tvName.setText(gArea.getName());
		
		// Set Distance
		Location mLoc = mLocManager.getLastKnownLocation(mGpsProvider);
		Location mLoc1 = mLocManager.getLastKnownLocation(mNetworkProvider);

		if (mLoc != null) {
			_distance = mLoc.distanceTo(TypeConverter.geoPointToLocation(gArea.getGeoPoint()));
			tvDist.setText(TypeConverter.formatDistance(_distance));
		} 
		else {
			if (mLoc1 != null) {
				_distance = mLoc1.distanceTo(TypeConverter.geoPointToLocation(gArea.getGeoPoint()));
				tvDist.setText(TypeConverter.formatDistance(_distance));				
			} else {
				tvDist.setText("Unknown distance...");			
			}
			
		}

		// Misc stuff
		gArea.getId();
		ParseId = gArea.getParseId();
		mMapActivityHelper.centerOnGuidedObject(_mMapView, gArea);

		gArea_write = gArea;
	}

	/*
	 * Area tapped info hide
	 */

	public void hideObjInfo() {
		rlObjectInfo.setVisibility(View.INVISIBLE);
	}

	
	
	
	/*****************************************************
	 * 
	 * 				ZOOM HANDLING
	 * 
	 *****************************************************/

	private int zoom;
	private ZoomListener zoomListener;

	public void setZoomLevel(int zoom) {
		this.zoom = zoom;
		_mMapView.getController().setZoom(zoom);
	}

	public ZoomListener getZoomListener() {
		return zoomListener;
	}

	public void setZoomListener(ZoomListener listener) {
		this.zoomListener = listener;
		_mMapView.getZoomButtonsController().setOnZoomListener(
				(OnZoomListener) this);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		boolean result = super.onTouchEvent(event);
		if (event.getAction() == MotionEvent.ACTION_UP) {
			checkForZoomEvent();
		}
		return result;
	}

	private void checkForZoomEvent() {
		if (this.zoomListener != null) {
			int newZoom = _mMapView.getZoomLevel();
			if (newZoom != zoom) {
				this.zoomListener.onZoom(zoom, newZoom);
			}
			zoom = newZoom;
		}
	}

	public void onZoom(boolean zoomIn) {
		int oldZoom = this.zoom;
		this.setZoomLevel(oldZoom + (zoomIn ? 1 : -1));
		if (this.zoomListener != null) {
			this.zoomListener.onZoom(oldZoom, this.zoom);
		}
	}

	@Override
	public void onZoom(int oldZoom, int newZoom) {
		if (newZoom >= 0 && newZoom <= 5)
			mZoomLevel = ZoomLevel.COUNTRY;
		if (newZoom > 5 && newZoom <= 10)
			mZoomLevel = ZoomLevel.CITY;
		if (newZoom > 10 && newZoom <= 15)
			mZoomLevel = ZoomLevel.AREA;
		if (newZoom > 15)
			mZoomLevel = ZoomLevel.AREAOBJ;
		initOverlays("0","1");
	}

	
	
	
	/*****************************************************
	 * 
	 * SCREEN ROTATION RELATED METHODS (Needed to handle screen rotation properly)
	 * 
	 *****************************************************/

	private void InitNuance() {

		// Create a single Vocalizer here and connect in advance

		_vocalizer = NuanceTtsHelper.getSpeechKit(this)
				.createVocalizerWithLanguage("en_US", _vocalizerListener,
						new Handler());
		_vocalizer.setVoice("Samantha");

		NuanceTtsHelper.getSpeechKit(this).connect();

		Log.i(TAG, "NUANCE VOCALIZER CREATED");
	}

	/*
	 * If this Activity is being recreated due to a config change (e.g. screen
	 * rotation), check for the saved state.
	 */

	private void RetrieveState(SavedState savedState) {

		mLocListener = savedState.mLocListener;
		mLocManager = savedState.mLocManager;
		
		//mUserLocOverlay = savedState.mUserLocOverlay;;
		//rlObjectInfo = savedState.rlObjectInfo;
		//_Listener = savedState.Listener;

		//mOverlays = savedState.mOverlays;
		//_mMapView = savedState.mMapView;
		mGuidedObjectItemizedOverlay = savedState.mGuidedObjectItemizedOverlay;

		_vocalizer = savedState.vocalizer;
		_lastTtsContext = savedState.context;
		//mMapActivityHelper = savedState.mMapActivityHelper;
		dontShowDialog = savedState.dontShowDialog;
		dialog = savedState.dialog;

	}
	
	/*
	 * Save the current state before rotation
	 */

	public Object onRetainNonConfigurationInstance() {
		// Save the Vocalizer state, because we know the Activity will be
		// immediately recreated.

		SavedState savedState = new SavedState();

		savedState.mLocListener = mLocListener;
		savedState.mLocManager = mLocManager;
		//savedState.mUserLocOverlay = mUserLocOverlay;
		//savedState.rlObjectInfo = rlObjectInfo;
		//savedState.Listener = _Listener;

		//savedState.mOverlays = mOverlays;
		//savedState.mMapView = _mMapView;
		savedState.mGuidedObjectItemizedOverlay = mGuidedObjectItemizedOverlay;

		savedState.vocalizer = _vocalizer;
		savedState.context = _lastTtsContext;
		
		savedState.dontShowDialog = dontShowDialog;
		savedState.dialog = dialog;
		//savedState.mMapActivityHelper = mMapActivityHelper;;
		_vocalizer = null; // Prevent onDestroy() from canceling
		return savedState;
	}

	

	
	
	
	/*****************************************************
	 *****************************************************
	 * ARROW ROTATING OVERLAY RELATED METHODS (Needed to handle screen rotation properly)
	 *****************************************************
	 *****************************************************/
		
	/*
	 * Class that represents the overlay arrow. Has method "draw" that is used to draw the arrow 
	 */
	
	public class OverlayArrow extends Overlay {
		
		//GeoPoint current;
		GeoPoint geostar = new GeoPoint(0,0);
		Location last;
		
		@Override public void draw(Canvas canvas, MapView mapv, boolean shadow){
			super.draw(canvas, mapv, shadow);
			if (!shadow) {
				Point point = new Point();
				Point center = new Point();
				Point endp = new Point();
				
				// To not to overload the phone we only ask for location in LocQualityCheck with the same period as for the GPS updates
				long current_time = System.currentTimeMillis();
				
				// ,GpsUpdateInterval was giving a too long delay between user location and the arrow on top
				if ((current_time - last_updated_time) > mGpsUpdateInterval/500) {
					
					last_updated_time = current_time;
				
					last = LocQualCheck();
					Log.d(TAG, " OverlayArrow.Draw");
				}
					
					if (last != null) {
						current = new GeoPoint((int)(last.getLatitude()*1E6),
								(int)(last.getLongitude()*1E6));
					}
				
				_mMapView.getProjection().toPixels(current, point);
				_mMapView.getProjection().toPixels(geostar, endp);
				center.set(point.x - 0, point.y - 0);
				drawPointer(canvas, center, getViewAngle());
/*				if (arrow) {
					Paint paint = new Paint();
					paint.setAntiAlias(true);
					paint.setStrokeCap(Cap.ROUND);
					paint.setStrokeWidth(2.0f);
					paint.setColor(0x60FF0000);
					paint.setPathEffect(new DashPathEffect(
							new float[] {10f, 10f}, phase));
					connectLine(canvas, paint, new Point(), center, endp);
				}
*/		
				// Icon change on arrow rotation is not yet implemented
				//canvas.drawBitmap(starplace, endp.x - (starplace.getWidth()/2), endp.y - starplace.getHeight(), null);

				phase -= 2.0f;
			}
		}
	}
	
	/*
	 * Methods that do not belong to above class OverlayArrow 
	 */
	
	public double getStarAngle(GeoPoint geostar, GeoPoint current) {
		double dy = geostar.getLatitudeE6() - current.getLatitudeE6();
		double dx = geostar.getLongitudeE6() - current.getLongitudeE6();
		return -Math.toDegrees(Math.atan2(dy, dx));
	}
	
	public static GeoPoint makePoint(double lat, double lon) {
		return new GeoPoint((int)(lat*1E6),(int)(lon*1E6));
	}
	
	public void fakeLocation(double latitude, double longitude) {
		current = makePoint(latitude, longitude);
		if (!dragging) {
			_mMapView.getController().setCenter(current);	
			
			// Updating the Location
			try {
				telnet.connect("10.0.2.2", 5554);
			} catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			PrintStream out = new PrintStream(telnet.getOutputStream());
			
                out.println("geo fix " + String.valueOf(longitude) + " "
                        + String.valueOf(latitude));
                out.flush();
                System.out.println("geo fix " + String.valueOf(longitude) + " "
                        + String.valueOf(latitude));
            
		}
		update();
		_mMapView.invalidate();
	}
	
	private void drag(boolean isdragging) {
		if (!isdragging) {
			((FrameLayout) _mMapView.getParent()).removeViewAt(1);
			dragging = false;
		} else {
			ImageButton personIcon = new ImageButton(this);
			personIcon.setImageResource(R.drawable.arrow);
			//personIcon.setImageResource(R.drawable.pointer);
			personIcon.setLayoutParams(new FrameLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
					Gravity.BOTTOM|Gravity.LEFT));
			personIcon.setOnClickListener(new OnClickListener() {
				@Override public void onClick(View v) {
					_mMapView.getController().animateTo(current, new Runnable() {
						public void run() { drag(false); }});
				}
			});
			FrameLayout frame = (FrameLayout) _mMapView.getParent();
			frame.addView(personIcon);
			dragging = true;
		}
		//onDrag(dragging);
	}
	
	@Override public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode==KeyEvent.KEYCODE_DPAD_UP) {
			lat += LOC_STEP;
			fakeLocation(lat,lon);
			return true;
		}
		if (keyCode==KeyEvent.KEYCODE_DPAD_DOWN) {
			lat -= LOC_STEP;
			fakeLocation(lat,lon);
			return true;
		}
		if (keyCode==KeyEvent.KEYCODE_DPAD_LEFT) {
			lon -= LOC_STEP;
			fakeLocation(lat,lon);
			return true;
		}
		if (keyCode==KeyEvent.KEYCODE_DPAD_RIGHT) {
			lon += LOC_STEP;
			fakeLocation(lat,lon);
			return true;
		}

		return super.onKeyDown(keyCode, event);
	}
	
	
	public double getViewAngle() {
		double angleTilt = Math.atan2(acc[1], acc[2]);
		double angleRot = Math.atan2(mag[0],mag[1]*Math.cos(angleTilt)-mag[2]*Math.sin(angleTilt));
		//Log.v("skot", "angle: "+angleRot);
		//Log.v("skot", "tilt: "+angleTilt);

		//if no compass sensor is present angle and tilt will be 0.0 so use simulated values
		if (angleRot == 0.0 && angleTilt == 0.0) {
			//Log.v("skot", "fake_compass: "+fake_compass);
			return (double)-90.0;
		}

		double result = -90.0-Math.toDegrees(angleRot);

		if (result < -180.0) {
			result += 360;
		}
		return result;
	}
	
	//draws the marker of the current positon and rotates it to the desired angle.
		//called every time map is invalidated.
		//skot
	private void drawPointer(Canvas canvas, Point center, double angle) {
		canvas.save(); //put canvas on temporary stack
		angle += 90; //rotate it 90 degrees right off the start so the icon lines up with the compass sensor
		//Log.v("skot", "center: " + center.x + "," + center.y);
		canvas.translate(center.x, center.y); //move upper left of canvas to center
		canvas.rotate((float) angle); //rotate canvas desired angle
		pointer.setBounds(-pointerWidth/2, -pointerHeight/2, pointerWidth/2, pointerHeight/2); //draw pointer to rotated canvas
		pointer.draw(canvas); //put pointer on canvas
		canvas.restore(); //restore canvas from temporary stack.

	}
	
	@Override public void onSensorChanged(SensorEvent event) {

		switch (event.sensor.getType()) {
		//switch (event.type) { //for compass simulator

		case Sensor.TYPE_ACCELEROMETER:
			//average(acc, event.values, 0.5f);
			return;
		case Sensor.TYPE_MAGNETIC_FIELD:
			average(mag, event.values, 0.5f);
			update();
			//Log.i("MAPVIEW INVALIDATE","CALLING FROM SENSOR_MAG_FIELD!");
			_mMapView.invalidate();
			return;
		}
	}

	static void average(float[] v1, float[] v2, float orig) {
		for (int i=0; i<3; i++) {
			v1[i] = orig*v1[i] + (1f-orig)*v2[i];
		}
	}
	
	public void update() {}
	
	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	/*****************************************************
	 ***************************************************** 
	 * Location Quality, Menu handling, Back-key pressed methods
	 *****************************************************
	 *****************************************************/
	
	
	
	public Location LocQualCheck () {
		
		Log.d(TAG, " LocQualCheck");
		
		Location loc;
		
		LocationManager myLocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Location LocGPS = myLocManager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		Location LocNtwrk = myLocManager
				.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

		// Check which location is better in terms of accuracy and "age"
		
		if (!EMULATOR) {
			if (LocQualityChecker.isBetterLocation(LocNtwrk, LocGPS)) {
				if (LocNtwrk != null){
					loc = LocNtwrk;
				}
				else {
					loc = TypeConverter.geoPointToLocation(current);
				}
			} else {
				if (LocGPS != null){
					loc = LocGPS;
				}
				else {
					loc = TypeConverter.geoPointToLocation(current);
				}
			}
		}
		else {
			loc = TypeConverter.geoPointToLocation(current);
		}
		
		Log.d(TAG, " Loc loc =" + loc);
		return loc;

	}
	
	//Menu Items Inflating
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			getMenuInflater().inflate(R.menu.home_menu_items, menu);
			super.onCreateOptionsMenu(menu);
			return true;
		}

		//What happens when menu item is selected
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			
			switch (item.getItemId()) {
	    
			case R.id.menu_settings:
	        	mMapActivityHelper.startActivity(SettingsActivity.class);
	            return true;
	        
			case R.id.menu_about:

				dialog = new Dialog(this);
				dialog.setContentView(R.layout.info);
				dialog.setTitle("Hi there!");
				
				
				ImageView logo = (ImageView) dialog.findViewById(R.id.ga_logo);
				
			    logo.setOnClickListener(new View.OnClickListener() {

			          @Override
			          public void onClick(View v) {
			        	  Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://guidedarea.com"));
			        	  startActivity(browserIntent);
			        	  
			        	  dialog.dismiss();
			          }

			        });
				
				
				TextView email = (TextView) dialog.findViewById(R.id.email_address);
				
				email.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						
						/* Create the Intent */
						final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

						/* Fill it with Data */
						emailIntent.setType("plain/text");
						emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"info@guidedarea.com"});
						emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Feedback about your app");
						emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Here's what I think about GuidedArea: \n");

						/* Send it off to the Activity-Chooser */
						startActivity(Intent.createChooser(emailIntent, "Send mail..."));
						dialog.dismiss();		
					}
				});
				
				Button dialogButton = (Button) dialog.findViewById(R.id.LeaveFeedback);
				
				dialogButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						
						Intent intent;
						intent = new Intent(getApplicationContext(), FeedbackActivity.class);
						startActivity(intent);
						dialog.dismiss();		
					}
				});
				
				dialog.show();
				return true;
				
			case R.id.menu_help:

				dialog = new Dialog(this);
				dialog.setContentView(R.layout.help);
				dialog.setTitle("Help");
				
				Button dialogButton1 = (Button) dialog.findViewById(R.id.have_fun);
				
				dialogButton1.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						
						dialog.dismiss();		
					}
				});
				
				dialog.show();
				return true;
			
			case R.id.menu_exit:
				setResult(RESULT_FIRST_USER);
				finish();
				return true;

			default:
	        	return mMapActivityHelper.onOptionsItemSelected(item)
						|| super.onOptionsItemSelected(item);
	    }  
			
			
		}
		


		@Override
		public void onBackPressed() {
			Bundle bundle = new Bundle();
			// bundle.putString(FIELD_A, mA.getText().toString());

			Intent mIntent = new Intent();
			mIntent.putExtras(bundle);
			setResult(RESULT_FIRST_USER, mIntent);
			super.onBackPressed();
		}
}
