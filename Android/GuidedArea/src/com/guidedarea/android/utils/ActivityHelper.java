package com.guidedarea.android.utils;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.guidedarea.R;
import com.guidedarea.android.ui.HomeActivity;
import com.guidedarea.android.ui.SettingsActivity;

/**
 * A class that handles some common activity-related functionality in the app,
 * such as setting up menu.
 */
public class ActivityHelper {

	private Activity mActivity;
	private Intent mIntent;
	private SharedPreferences mPreferences;
	private String TAG = "ActivityHelper";

	/**
	 * Factory method for creating {@link ActivityHelper} objects for a given
	 * activity.
	 */
	public static ActivityHelper createInstance(Activity activity) {
		return new ActivityHelper(activity);
	}

	protected ActivityHelper(Activity activity) {
		mActivity = activity;
		Log.i(TAG, "" + mActivity);
		mPreferences = PreferenceManager.getDefaultSharedPreferences(mActivity);
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		mActivity.getMenuInflater().inflate(R.menu.default_menu_items, menu);
		return false;
	}

	public boolean onCreateOptionsMenu(Menu menu, int id) {
		mActivity.getMenuInflater().inflate(id, menu);
		return false;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_about:
			makeToast("About selected");
			return true;
		case R.id.menu_exit:
			makeToast("Exit selected");
			return true;
		case R.id.menu_help:
			makeToast("Help selected");
			return true;
		case R.id.menu_home:
			startActivity(HomeActivity.class);
			return true;
		case R.id.menu_list:
			makeToast("List selected");
			return true;
		case R.id.menu_settings:
			makeToast("Settings selected");
			startActivity(SettingsActivity.class); //TODO: CHANGE!!!
			return true;
		}
		return false;
	}

	private void makeToast(String message) {
		Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
	}

	public void startActivity(Class<?> activityClass) {
		mIntent = new Intent(mActivity, activityClass);
		mActivity.startActivity(mIntent);
	}

	public void startActivityForResult(Class<?> activityClass) {
		mIntent = new Intent(mActivity, activityClass);
		mActivity.startActivity(mIntent);
	}

	public SharedPreferences getPreferences() {
		return mPreferences;
	}

	public void checkLogin() {
		// TODO:this nethod should check if user logged in; if not redirect to
		// LoginActivity
	}
}
