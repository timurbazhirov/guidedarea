package com.guidedarea.android.utils;

public enum GuideType {
	AUDIO, VISUAL;
}
