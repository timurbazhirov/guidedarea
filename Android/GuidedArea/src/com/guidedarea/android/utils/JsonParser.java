package com.guidedarea.android.utils;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.location.Location;
import android.util.Log;

import com.google.android.maps.GeoPoint;
import com.guidedarea.android.models.GArea;

/***
 * 
 * @author Timur Bazhirov: parser for server output
 * 
 */

public class JsonParser {

	/**
	 * Get Area from JSON
	 */

	public static GArea getArea(JSONObject areaJson) throws JSONException {

		Log.i(JsonParser.class.getName(), "getArea(JSONObject areaJson)");

		String description = null, wikiLink = null, photoLink = null, websiteLink = null, address = null, name = null, pointsArray = null, objIdsJson = null;

		int latitude, longitude;
		int id = -1;
		int[] areaObjIds = null;
		String ParseId = null;

		// Required properties
		latitude = (int) (areaJson.getDouble("latitude") * 1000000);
		longitude = (int) (areaJson.getDouble("longitude") * 1000000);
		name = areaJson.getString("name");
		id = areaJson.getInt("id");
		ParseId = areaJson.getString("objectId");

		// Optional properties
		try {
			description = areaJson.getString("description");
		} catch (JSONException e) {
			Log.i(JsonParser.class.getSimpleName(),
					"getting 'description' failed");
		}

		try {
			wikiLink = areaJson.getString("wiki_link");
		} catch (JSONException e) {
			Log
					.i(JsonParser.class.getSimpleName(),
							"getting 'wikiLink' failed");
		}

		try {
			photoLink = areaJson.getString("photo_link");
		} catch (JSONException e) {
			Log.i(JsonParser.class.getSimpleName(),
					"getting 'photoLink' failed");
		}

		try {
			websiteLink = areaJson.getString("website_link");
		} catch (JSONException e) {
			Log.i(JsonParser.class.getSimpleName(),
					"getting 'websiteLink' failed");
		}
		try {
			address = areaJson.getString("address");
		} catch (JSONException e) {
			Log.i(JsonParser.class.getSimpleName(), "getting 'address' failed");
		}
		try {
			objIdsJson = areaJson.getString("objects");
		} catch (JSONException e) {
			Log.i(JsonParser.class.getSimpleName(),
					"getting 'objIdsJson' failed");
		}
		try {
			pointsArray = areaJson.getString("points");
		} catch (JSONException e) {
			Log.i(JsonParser.class.getSimpleName(),
					"getting 'pointsArray' failed");
		}

		/*
		 * Get Borders depending on perimeter points
		 */

		Location[] border = null;
		if (pointsArray != null) {
			try {
				JSONArray points = new JSONArray(pointsArray);
				border = new Location[points.length()];
				for (int j = 0; j < points.length(); j++) {
					Location loc = new Location((String) null);
					JSONObject point = points.getJSONObject(j);
					loc.setLatitude(point.getDouble("lat"));
					loc.setLongitude(point.getDouble("long"));
					border[j] = loc;
				}
			} catch (JSONException e) {
				Log.i(JsonParser.class.getSimpleName(),
						"Error during border points parsing: " + e.toString());
			}
		}

		if (objIdsJson != null) {
			try {
				JSONArray objects = new JSONArray(objIdsJson);
				areaObjIds = new int[objects.length()];
				for (int j = 0; j < objects.length(); j++) {
					JSONObject object = objects.getJSONObject(j);
					areaObjIds[j] = object.getInt("id");
				}
			} catch (JSONException e) {
				Log.i(JsonParser.class.getSimpleName(),
						"Error during convert jsonArray to array");
			}
		}

		GArea ga = (new GArea.Builder(id, name, new GeoPoint(latitude,
				longitude))).ParseId(ParseId).description(description).address(
				address).areaObjIds(areaObjIds).border(border).website(
				websiteLink).wikiLink(wikiLink).photoLink(photoLink).build();
		return ga;

	}

	/**
	 * Get Area from String
	 */

	public static GArea getArea(String jsonData) throws JSONException {
		Log.i(JsonParser.class.getName(), "getArea(String jsonData)");
		JSONObject areaJson = new JSONObject(jsonData);
		return getArea(areaJson);
	}

	/**
	 * Get Areas List from String
	 */

	public static List<GArea> getAreasList(String jsonData)
			throws JSONException {

		Log.i(JsonParser.class.getName(), "getAreasList(String jsonData)");
		List<GArea> gAreas = new ArrayList<GArea>();

		JSONArray gAreasJsonArray = new JSONArray(jsonData);

		for (int i = 0; i < gAreasJsonArray.length(); i++) {
			Log.i(JsonParser.class.getSimpleName(), gAreasJsonArray.get(i)
					.toString());
			gAreas.add(getArea(gAreasJsonArray.get(i).toString()));
		}
		return gAreas;
	}

	/**
	 * Get AreaObject from JSON
	 */

}
