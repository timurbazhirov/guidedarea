package com.guidedarea.android.utils;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.guidedarea.android.models.GArea;
import com.guidedarea.android.ui.SightseeingMapActivity;

public class MapActivityHelper extends ActivityHelper {

	private String mGpsProvider = LocationManager.GPS_PROVIDER;
	private String mNetworkProvider = LocationManager.NETWORK_PROVIDER;
	private Location mGPSLoc, mNetLoc;
	private String TAG = "######### GUIDED AREA APPLICATION ######### MapActivityHelper";

	protected MapActivityHelper(Activity activity) {
		super(activity);
	}

	/**
	 * Factory method for creating {@link ActivityHelper} objects for a given
	 * activity.
	 */
	public static MapActivityHelper createInstance(Activity activity) {
		return new MapActivityHelper(activity);
	}

	public void initMapView(MapView mapView) {
		mapView.setBuiltInZoomControls(true);
	}

	// Method to query phone location and center map on that location. If GPS is enabled, uses it, if not - uses network.
	public void centerOnLocation(MapView mapView, LocationManager locManager) {
		mGPSLoc = locManager.getLastKnownLocation(mGpsProvider);
		mNetLoc = locManager.getLastKnownLocation(mNetworkProvider);
		Log.i(TAG, "GPS LOCATION = " + mGPSLoc + " " + "NETWORK LOCATION = " + mNetLoc);
		if (mGPSLoc != null) {
			GeoPoint newPoint = TypeConverter.locationToGeoPoint(mGPSLoc);
			mapView.getController().animateTo(newPoint);
		}
		else{
			if (mNetLoc != null){
				GeoPoint newPoint = TypeConverter.locationToGeoPoint(mNetLoc);
				mapView.getController().animateTo(newPoint);
			}
			else{
				GeoPoint newPoint = SightseeingMapActivity.current;
				mapView.getController().animateTo(newPoint);
			}
		}
	}

	public void centerOnGuidedObject(MapView mapView, GArea gArea) {
		Log.i(TAG, "Centered on " + gArea.getGeoPoint().toString());
		if (gArea != null) {
			mapView.getController().animateTo(gArea.getGeoPoint());
		}

	}
	


}
