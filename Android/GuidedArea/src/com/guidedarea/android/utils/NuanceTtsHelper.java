package com.guidedarea.android.utils;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.guidedarea.R;
import com.nuance.nmdp.speechkit.Prompt;
import com.nuance.nmdp.speechkit.SpeechError;
import com.nuance.nmdp.speechkit.SpeechKit;
import com.nuance.nmdp.speechkit.Vocalizer;

public class NuanceTtsHelper {

	private static String TAG = "########### NuanceTtsHelper #############";

	private static SpeechKit _speechKit;

	public static final String SpeechKitServer = "sandbox.nmdp.nuancemobility.net";

	public static final int SpeechKitPort = 443;

	public static final String SpeechKitAppId = "NMDPTRIAL_radikft20110919122358";
	public static final byte[] SpeechKitApplicationKey = { (byte) 0x95,
			(byte) 0x4b, (byte) 0xf6, (byte) 0x39, (byte) 0x11, (byte) 0xf6,
			(byte) 0x45, (byte) 0x02, (byte) 0xa9, (byte) 0xea, (byte) 0x25,
			(byte) 0x49, (byte) 0x5a, (byte) 0x6e, (byte) 0x18, (byte) 0x0c,
			(byte) 0x37, (byte) 0xcd, (byte) 0x5c, (byte) 0x12, (byte) 0x86,
			(byte) 0x69, (byte) 0xf4, (byte) 0x4e, (byte) 0x23, (byte) 0x52,
			(byte) 0xf5, (byte) 0x48, (byte) 0x3e, (byte) 0x46, (byte) 0x07,
			(byte) 0xd3, (byte) 0x92, (byte) 0xfd, (byte) 0x0c, (byte) 0xa3,
			(byte) 0xf4, (byte) 0x52, (byte) 0xa5, (byte) 0x9d, (byte) 0x3c,
			(byte) 0xca, (byte) 0xe7, (byte) 0x97, (byte) 0x38, (byte) 0xff,
			(byte) 0xa3, (byte) 0x6f, (byte) 0xcc, (byte) 0xff, (byte) 0x82,
			(byte) 0x53, (byte) 0x68, (byte) 0x76, (byte) 0xa1, (byte) 0x8c,
			(byte) 0xc9, (byte) 0x96, (byte) 0xd4, (byte) 0x07, (byte) 0xdc,
			(byte) 0xdc, (byte) 0xdb, (byte) 0x6e };

	public static final String TTS_KEY = "com.nuance.nmdp.sample.tts";

	private Vocalizer _vocalizer = null;
	private Object _lastTtsContext = null;
	private static NuanceTtsHelper mInstance = null;
	private Vocalizer.Listener vocalizerListener = null;
	private boolean isSpeaking = false;

	private NuanceTtsHelper(Context context) {
		_speechKit = SpeechKit.initialize(context.getApplicationContext(),
				SpeechKitAppId, SpeechKitServer, SpeechKitPort,
				SpeechKitApplicationKey);
		_speechKit.connect();
		// TODO: Keep an eye out for audio prompts not working on the Droid 2 or
		// other 2.2 devices.
		Prompt beep = _speechKit.defineAudioPrompt(R.raw.beep);
		_speechKit.setDefaultRecognizerPrompts(beep, Prompt.vibration(100),
				null, null);

		createVocalizer();

	}

	public static SpeechKit getSpeechKit(Context context) {
		if (_speechKit == null) {
			_speechKit = SpeechKit.initialize(context.getApplicationContext(),
					SpeechKitAppId, SpeechKitServer, SpeechKitPort,
					SpeechKitApplicationKey);
			_speechKit.connect();
			// TODO: Keep an eye out for audio prompts not working on the Droid
			// 2 or other 2.2 devices.
			Prompt beep = _speechKit.defineAudioPrompt(R.raw.beep);
			_speechKit.setDefaultRecognizerPrompts(beep, Prompt.vibration(100),
					null, null);
		}
		return _speechKit;
	}

	public void createVocalizer() {
		_vocalizer = _speechKit.createVocalizerWithLanguage("en_US",
				createVocalizerListener(), new Handler());
		_vocalizer.setVoice("Samantha");
	}

	public static NuanceTtsHelper createInstance(Context context) {
		if (mInstance == null) {
			mInstance = new NuanceTtsHelper(context);
			Log.i(TAG + " AreaExploreActivity", "Return new Nuance");
		} else {
			Log.i(TAG + " AreaExploreActivity", "Return old Nuance");
		}
		return mInstance;
	}

	public void onDestroy() {
		if (_vocalizer != null) {
			_vocalizer.cancel();
			_vocalizer = null;
		}
		if (_speechKit != null) {
			_speechKit.release();
			_speechKit = null;
		}
	}

	public void startSpeech(String stext) {
		//_lastTtsContext = new Object();
		if (stext != null) {
			_vocalizer.speakString(stext, _lastTtsContext);
			//_vocalizer.speakMarkupString(stext, _lastTtsContext);
			
		} else {
			_vocalizer.speakString("There is no information about this object",
					_lastTtsContext);
		}
	}
	
	public void startMSpeech(String stext) {
		//_lastTtsContext = new Object();
		if (stext != null) {
			//_vocalizer.speakString(stext, _lastTtsContext);
			_vocalizer.speakMarkupString(stext, _lastTtsContext);
			
		} else {
			_vocalizer.speakString("There is no information about this object",
					_lastTtsContext);
		}
	}

	public void setVocalizer(Vocalizer v) {
		_vocalizer = v;
	}

	public Vocalizer getVocalizer() {
		return _vocalizer;
	}

	public boolean getStatus() {
		return isSpeaking;
	}
	
	public Vocalizer.Listener getVocalizerListener() {
		if (vocalizerListener == null) {
			return new Vocalizer.Listener() {
				@Override
				public void onSpeakingBegin(Vocalizer vocalizer, String text,
						Object context) {
				}

				@Override
				public void onSpeakingDone(Vocalizer vocalizer, String text,
						SpeechError error, Object context) {
					// Use the context to detemine if this was the final TTS
					// phrase
					if (context != _lastTtsContext) {
					} else {
						
					}
				}
			};
		} else {
			return vocalizerListener;
		}
	}

	public Vocalizer.Listener createVocalizerListener() {

		return new Vocalizer.Listener() {
			@Override
			public void onSpeakingBegin(Vocalizer vocalizer, String text,
					Object context) {
				}

			@Override
			public void onSpeakingDone(Vocalizer vocalizer, String text,
					SpeechError error, Object context) {
				// Use the context to detemine if this was the final TTS phrase
				if (context != _lastTtsContext) {
				} else {
				}
			}
		};

	}

}
