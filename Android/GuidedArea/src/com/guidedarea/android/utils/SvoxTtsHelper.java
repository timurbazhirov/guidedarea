package com.guidedarea.android.utils;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.util.Log;

import com.svox.classic.TTS;
import com.svox.classic.TTS.TTSSpeechData;

public class SvoxTtsHelper implements TTSSpeechData {
	private TTS mSOfA;
	private long mLingwareHandle;
	private long mEngineHandle;
	private long mChannelHandle;
	private AudioTrack mAudio;
	private String TAG = "SvoxTtsHelper";
	private String mText;
	private static SvoxTtsHelper mInstance = null;

	private SvoxTtsHelper(final String text) {
		mText = text;
		try {
			mAudio = new AudioTrack(AudioManager.STREAM_MUSIC, 22050,
					AudioFormat.CHANNEL_CONFIGURATION_MONO,
					AudioFormat.ENCODING_PCM_16BIT, 4096,
					AudioTrack.MODE_STREAM);
		} catch (IllegalArgumentException argex) {
			// error handling
		}

		try {
			mSOfA = new TTS("/sdcard/talkingandroid/", this);
			mLingwareHandle = mSOfA.loadLingware("/sdcard/talkingandroid/",
					"svox-mh5pt2en-US22.pil");
			mEngineHandle = mSOfA.newEngine("/sdcard/talkingandroid/");
			mChannelHandle = mSOfA.newChannel(mEngineHandle, "");

		} catch (RuntimeException ex) {
			Log.e(TAG, ex.getMessage());
		}
	}

	public static SvoxTtsHelper createInstance(final String text) {
		if (mInstance == null) {
			mInstance = new SvoxTtsHelper(text);
		}
		return mInstance;
	}

	@Override
	public void onSpeechData(short[] samples) {
		mAudio.play();
		mAudio.write(samples, 0, samples.length);
		mAudio.stop();

	}

	@Override
	public void onSpeechDone(int arg0) {
		// TODO Auto-generated method stub

	}

	public void onDestroy() {
		mSOfA.closeChannel(mEngineHandle, mChannelHandle);
		mSOfA.closeEngine(mEngineHandle);
		mSOfA.unloadLingware(mLingwareHandle);
	}

	public int startSpeech() {
		return mSOfA.synthString(mEngineHandle, mChannelHandle, mText);
	}

}
