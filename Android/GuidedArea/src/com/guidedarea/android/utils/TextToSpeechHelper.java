package com.guidedarea.android.utils;

import java.util.Locale;

import com.guidedarea.android.ui.SightseeingMapActivity;

import android.app.Activity;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;
import android.widget.Toast;

public class TextToSpeechHelper implements OnInitListener, OnUtteranceCompletedListener {
	public static final int TTS_DATA_CHECK_CODE = 2374;
	private static TextToSpeech mTts;
	private Activity mActivity;
	private int QueueMode = TextToSpeech.QUEUE_FLUSH;

	public static TextToSpeechHelper createInstance(Activity activity) {
		return new TextToSpeechHelper(activity);
	}

	protected TextToSpeechHelper(Activity activity) {
		mActivity = activity;
		mTts = new TextToSpeech(mActivity, this);
	}

	public void startActivityResult() {
		Intent checkIntent = new Intent();
		checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
		mActivity.startActivityForResult(checkIntent, TTS_DATA_CHECK_CODE);
	}

	public void onActivityResult(int requestCode, int resultCode,
			Intent data) {
		if (requestCode == TTS_DATA_CHECK_CODE) {
			if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
				// success, create the TTS instance
				mTts = new TextToSpeech(mActivity, this);
			} else {
				// missing data, install it
				Intent installIntent = new Intent();
				installIntent
						.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
				mActivity.startActivity(installIntent);
			}
		}
	}

	@Override
	public void onInit(int status) {
		if (status == TextToSpeech.SUCCESS) {
			//Toast.makeText(mActivity, "Text-To-Speech engine is initialized",
					//Toast.LENGTH_LONG).show();
			mTts.setLanguage(Locale.US);
			mTts.setOnUtteranceCompletedListener(this);
		} 
		else if (status == TextToSpeech.ERROR) {
			Toast.makeText(mActivity,
					"Error occurred while initializing Text-To-Speech engine",
					Toast.LENGTH_LONG).show();
		}

	}

	public void startSpeech(String text, boolean Buttonshow) {
		// TODO Auto-generated method stub
		if (text != null) {
			mTts.speak(text, QueueMode, null);
			if (Buttonshow)	SightseeingMapActivity.ShowStop();
		} else {
			mTts.speak("There is no information about this object", QueueMode,
					null);
		}
	}
	
	public TextToSpeech getTTS() {
		return mTts;
	}
	
	public boolean IsSpeaking() {
		return mTts.isSpeaking();
	}
	
	public void Stop() {
		if (mTts != null) {
            mTts.stop();
        }
	}

	public void onDestroy() {
		// TODO Auto-generated method stub
		//mTts.shutdown(); - commented to avoid a forseclosure when navigating back from guide screen when audiolocationlistener fired
	        if (mTts != null) {
	            mTts.stop();
	            mTts.shutdown();
	        }	 
	       // super.onDestroy();
	}

	@Override
	public void onUtteranceCompleted(String utteranceId) {
			// Hide StopTSS button in SightSeeingMap Activity
			SightseeingMapActivity.HideStop();
	}

}
