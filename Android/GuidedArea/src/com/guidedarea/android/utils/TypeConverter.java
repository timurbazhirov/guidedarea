package com.guidedarea.android.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

import android.location.Location;

import com.google.android.maps.GeoPoint;

public class TypeConverter {

	public static GeoPoint locationToGeoPoint(Location mLocation) {
		double mLat = mLocation.getLatitude();
		double mLon = mLocation.getLongitude();
		GeoPoint newPoint = new GeoPoint((int) (mLat * 1e6), (int) (mLon * 1e6));
		return newPoint;
	}

	public static Location geoPointToLocation(GeoPoint geoPoint) {
		Location loc = new Location("");
		loc.setLatitude(geoPoint.getLatitudeE6() / 1e6);
		loc.setLongitude(geoPoint.getLongitudeE6() / 1e6);
		return loc;
	}

	public static String formatDistance(float meters) {
		double km = new BigDecimal(meters / 1000.).setScale(1, RoundingMode.UP)
				.doubleValue();
		if (km >= 1.) {
			return "" + km + " km";
		} else {
			return ""
					+ new BigDecimal(meters).setScale(1, RoundingMode.UP)
							.doubleValue() + " m";
		}
	}

}
