package com.guidedarea.android.utils;

public class ZoomHelper {

	public static ZoomLevel DetectZoom(int i) {

		ZoomLevel mZoomLevel = ZoomLevel.AREA;

		if (i >= 0 && i <= 5)
			mZoomLevel = ZoomLevel.COUNTRY;
		if (i > 5 && i <= 10)
			mZoomLevel = ZoomLevel.CITY;
		if (i > 10 && i <= 15)
			mZoomLevel = ZoomLevel.AREA;
		if (i > 15)
			mZoomLevel = ZoomLevel.AREAOBJ;
		return mZoomLevel;
	}
}
