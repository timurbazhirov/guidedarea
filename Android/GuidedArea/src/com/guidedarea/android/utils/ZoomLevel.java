package com.guidedarea.android.utils;

public enum ZoomLevel {
	COUNTRY, CITY, AREA, AREAOBJ
}