#!/bin/bash

rm -rf */gen
rm -rf */*/gen
rm -rf */*/*/gen
rm *~

rm -rf */bin
rm -rf */*/bin
rm -rf */*/*/bin
rm *~