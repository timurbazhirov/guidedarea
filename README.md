guidedarea
==========

mobile/web platform for interactive walking tours

/Android - has the Android App sourcecode

/Scraper - Python Scraper populating the database (Parse.com-based) for the front-end clients (now Android only)

/Website_PlayFramework - current website draft http://guidedarea.com

More info on the github Wiki page: https://github.com/timurbazhirov/guidedarea/wiki

Contact timur@bazhirov.com with any questions