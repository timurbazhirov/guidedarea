# -*- coding: utf-8 -*-
# lat, lon = "0°25\'30\"S, 91°7\'W\".split(', ')
# Converts GPS coordinates in degrees into decimals #
# print conversion(lat), conversion(lon)
# #Output:
# 0.425 91.1166666667

def conversion(old):
#    print "IN CONVERSION, STRING = ", old
    direction = {'N':-1, 'S':1, 'E': -1, 'W':1}
    new = old.replace('°',' ').replace('′',' ').replace('″',' ')
#    print "IN CONVERSION, NEW = ", new
    new = new.split()
    new_dir = new.pop()
    new.extend([0,0,0])
    return (float(new[0])+float(new[1])/60.0+float(new[2])/3600.0) * direction[new_dir]

#print conversion("122°15′28.34″W")
