Guided Area
Python scripts for wikipedia crawling
ver. 11 Sep 2012

Way to use:
1. Use Openstreetmaps.com to generate osm data file in xml format with all map information in a relatively small (city downtown, university campus etc) area (example xml output is given for UC Berkeley campus)
2. Use scraper.py to create json array with border geopoints and wikipedia-taken-from description
2a. Right now "places.xml" file is used as a backup database for scraper as it has much info about for UCB campus
3. Use UPLOADtoPARSE to upload the content into parse.com
