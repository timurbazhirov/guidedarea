package controllers;

import models.Bookmarks;

import com.google.gson.JsonObject;

import play.Logger;
import play.cache.Cache;
import play.modules.facebook.FbGraph;
import play.modules.facebook.FbGraphException;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Scope.Session;

public class Bookmark extends Controller {
	public static void index(String media, String url, String height, String width) {
		Logger.info("##Bookmark.index##");
		Logger.info("image media--> %s", media);
		Logger.info("image width--> %s", width);
		Logger.info("image height--> %s", height);
		Logger.info("website url--> %s", url);
		
		double heightInDouble = new Double(height).doubleValue();
		double widthInDouble = new Double(width).doubleValue();
		double aspect = widthInDouble/heightInDouble;
		
		long widthForBookmark, heightForBookmark;
		
		if (widthInDouble > heightInDouble) {
	         widthForBookmark = 420;
	         heightForBookmark= Math.round(420 / aspect);
	    }
	    else {
	    	heightForBookmark = 440;
	        widthForBookmark = Math.round(440 * aspect);
	    }
		
		renderArgs.put("media", media);
		renderArgs.put("heightForBookmark", new Long(heightForBookmark).toString());
		renderArgs.put("widthForBookmark", new Long(widthForBookmark).toString());
		renderArgs.put("actualwidth", width);
		renderArgs.put("actualheight", height);
		renderArgs.put("websiteurl", url);

		renderTemplate("Bookmark/bookmarkImage.html");		
    }
	
	
	
	public static void saveComment(String comment, String mediaurl, String actualheight, String actualwidth, String websiteurl) {
		Logger.info("##Bookmark.saveComment##");
		Logger.info("In save comment");
		Logger.info("comment received-->" + comment);
		Logger.info("mediaurl received -->" + mediaurl);
		Logger.info("actual height-->" + actualheight);
		Logger.info("actual width -->" + actualwidth );
		Logger.info("website url -->" + websiteurl );
		
		//Persist in database.
		Bookmarks imageBookmark = new Bookmarks(
				Session.current().get("username"),
				Session.current().get("email"),
				mediaurl,
				new Integer(actualheight).intValue(),
				new Integer(actualwidth).intValue(),
				comment,
				websiteurl).save();
		
        renderText("Completed Save");	
    }
	
	@Before(only={"index","saveComment"})
    static void checkAuthentification() {
		Logger.info("##Bookmark.checkAuthentification##");
		Logger.info("checking for a valid session-->");
        
		//Check if the username retrieved from Facebook exists in the session
		if(Session.current().get("email")==null || Session.current().get("username")==null) {
			//Save the media and url to the cache.
			Cache.set(session.getId()+"-media", params.get("media"));
			Cache.set(session.getId()+"-url", params.get("url"));
			Cache.set(session.getId()+"-height", params.get("height"));
			Cache.set(session.getId()+"-width", params.get("width"));
			
			Logger.info("No valid session exists..redirecting to login page");
			renderTemplate("Bookmark/index.html");		
		}
        else {
        	Logger.info("valid session exists..");
        }	
    }
	
	 public static void facebookLogin() {
		Logger.info("##Bookmark.facebookLogin##"); 
        try {
            JsonObject profile = FbGraph.getObject("me"); // fetch the logged in user
            String email = profile.get("email").getAsString(); // retrieve the email
            String name = profile.get("name").getAsString();
            
            Logger.info("Email address--> %s", email);
            Logger.info("Name -->%s", name);
            
            // do useful things
            Session.current().put("email", email); // put the email into the session (for the Secure module)
            Session.current().put("username", name); // put the name into the session  
        } catch (FbGraphException fbge) {
            flash.error(fbge.getMessage());
            if (fbge.getType() != null && fbge.getType().equals("OAuthException")) {
                Session.current().remove("username");
                Session.current().remove("email");
            }
        }
        //Get media & url from cache and forward to index action.
        index((String)Cache.get(session.getId()+"-media"),(String)Cache.get(session.getId()+"-url"),
        		(String)Cache.get(session.getId()+"-height"),(String)Cache.get(session.getId()+"-width"));
	 }
	 
	 public static void bookmarkAsGuest() {
		 Logger.info("##Bookmark.bookmarkAsGuest##"); 
		 
		// Put username and email address in session.
         Session.current().put("email", params.get("email")); // put the email into the session (for the Secure module)
         Session.current().put("username", params.get("username")); // put the name into the session  
         Session.current().put("loginasguest", "true");
         
       //Get media & url from cache and forward to index action.
         index((String)Cache.get(session.getId()+"-media"),(String)Cache.get(session.getId()+"-url"),
         		(String)Cache.get(session.getId()+"-height"),(String)Cache.get(session.getId()+"-width"));
	 }
}
