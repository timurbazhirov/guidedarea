package controllers;

import play.*;
import play.modules.facebook.FbGraph;
import play.modules.facebook.FbGraphException;
import play.mvc.*;
import play.mvc.Scope.Session;
import play.libs.*;
import play.libs.WS;
import play.libs.WS.*;
import services.HashGenerator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import models.*;

public class DownloadLogin extends Controller {

    public static void index() {
		Logger.info("##DownloadLogin.index##");     	
        render();
    }
    
    public static void facebookLogin() {
		Logger.info("##DownloadLogin.facebookLogin##"); 
        try {
            JsonObject profile = FbGraph.getObject("me"); // fetch the logged in user
            String email = profile.get("email").getAsString(); // retrieve the email
            String atoken = FbGraph.getAccessToken();
            String name = profile.get("name").getAsString();
            //String givenname = profile.get("givenName").getAsString();
            //String familyname = profile.get("familyName").getAsString();
            String id = profile.get("id").getAsString(); // retrieve the email
            //JsonObject user = FbGraph.getObject(, Parameter.with("access_token", "YOUR_ACCESS_TOKEN").parameters());
            //String access_token = FbGraph.checkAuthentification. access_token;
            //profile.get("access_token").getAsString(); // retrieve the email
            //String expiration_date = profile.get("expiration_date").getAsString(); // retrieve the email
            String expiration_date = "2012-11-07T20:58:34.448Z"; //FbGraph.getExpDate();
            
 

            Logger.info("Email address--> %s", email);
            Logger.info("Name -->%s", name);
            //Logger.info("Name -->%s", givenname);
            //Logger.info("Name -->%s", familyname);
            Logger.info("Name -->%s", id);
            Logger.info("Name -->%s", atoken);
            //Logger.info("Name --  >%s", access_token);
            Logger.info("Exp date -->%s", expiration_date);
            
            // do useful things
            Session.current().put("email", email); // put the email into the session (for the Secure module)
            Session.current().put("username", name); // put the name into the session 
            
            parseStore(id, atoken, expiration_date, email, name);
            

        } catch (FbGraphException fbge) {
            flash.error(fbge.getMessage());
            if (fbge.getType() != null && fbge.getType().equals("OAuthException")) {
                Session.current().remove("username");
                Session.current().remove("email");
            }
        }
        index();
    }

    public static void parseStore(String _id, String atoken, String _expiration_date, String email, String name) {
        Logger.info("##DownloadLogin.parseStore##"); 
            String url = "http://api.parse.com/1/users";
            String charset = "ISO-8859-1";

            String param1 = "void";
            String appid = "bAMGYsME9tgqpa4uIeeT49FgsgMrUbl8pKbirrmK";
            String restapikey = "7W88qLyGJtfEIlGcKo2vaOvfXhGwDHWaJFT7IcNz";
            String contenttype = "application/json";

            class _AuthData {
              private String id;
              private String access_token;
              private String expiration_date;
              _AuthData(String _id, String atoken, String _expiration_date) {
                id = _id;
                access_token = atoken;
                expiration_date = _expiration_date;
                // no-args constructor
              }
            }

            class AuthData2 {
              private String id = "2342";
              private String access_token = "asfasdf";
              private String expiration_date = "2349872";
              AuthData2() {
                // no-args constructor
              }
            }

            AuthData2 obj = new AuthData2(); //(_id, atoken, _expiration_date);
            Gson gson = new Gson();
            String facebook = gson.toJson(obj);
            String AuthData = gson.toJson(facebook);


            Logger.info("obj -->%s", facebook);
            Logger.info("Exp date -->%s", AuthData);

            String body = "{\"authData\":{\"facebook\":{\"id\":\"" + _id + "\",\"access_token\":\"" + atoken + "\",\"expiration_date\":\"" + _expiration_date + "\"}}}";
            Logger.info("Exp date -->%s", body);


            WSRequest wsRequest = WS.url(url);
            wsRequest.body = body;
            wsRequest.headers.put("X-Parse-Application-Id", appid);
            wsRequest.headers.put("X-Parse-REST-API-Key", restapikey);
            wsRequest.headers.put("Content-Type", contenttype); //mimetype
            HttpResponse wsResponse = wsRequest.post();
            String responseString = wsResponse.getString(charset);

            Logger.info("Email address stored --> %s", responseString);
            //Logger.info("Name stored -->%s", name);

            redirect("/Download");
    }
    
    public static void facebookLogout() {
        Logger.info("##DownloadLogin.facebookLogout##"); 
        Session.current().remove("username");
        Session.current().remove("name");
        FbGraph.destroySession();
        redirect("/login");
    }
    
    //@Before(only={"index"})
    static void checkAuthentification() {
		Logger.info("##DownloadLogin.checkAuthentification##");     	
		Logger.info("checking for a valid session-->");
        
		//Check if the username retrieved from Facebook exists in the session
		if(Session.current().get("email")==null || Session.current().get("username")==null) {
			Logger.info("No valid session exists..redirecting to login page");
			redirect("/login");			
		}
        else {
        	Logger.info("valid session exists..");
        }	
    }
}