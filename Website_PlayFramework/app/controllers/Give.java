package controllers;

import play.*;
import play.cache.Cache;
import play.modules.facebook.FbGraph;
import play.mvc.*;
import play.mvc.Scope.Session;

import java.util.*;

import models.*;

public class Give extends Controller {
	
    public static void index() {
    	Logger.info("##Give.index##");
    	Logger.info("id-->" + params.get("id"));
    	Logger.info("in Give index");
    	
    	List<Comment> listOfComments = Comment.find("hash = ? order by id desc",params.get("id")).fetch(1);
    	if(listOfComments!=null && listOfComments.size() >= 1) {
    		renderArgs.put("question", listOfComments.get(0));
    		render();
    	}else {
    		Question question = Question.find("byHash",params.get("id")).first();
        	if(question != null ) {
        		renderArgs.put("question", question);
        		render();
        	}	
        	else
        		redirect("/ask");
    	}
    }
    
    public static void saveComment() {
    	Logger.info("##Give.saveComment##");
    	Logger.info("id-->" + params.get("id"));
    	   	
    	Comment comment = new Comment(
    					params.get("question"),
    					params.get("friendsEmailAddresses"),
    					params.get("id"),
    					Session.current().get("email"),
    					Session.current().get("username"),
    					params.get("firstprod"),
    					params.get("secondprod"),
    					params.get("thirdprod")).save();
    }
    
    //@Before(only={"index"})
    static void checkAuthentification() {
    	Logger.info("##Give.checkAuthentification##");
		Logger.info("checking for a valid session in give-->");
        
		//Check if the username retrieved from Facebook exists in the session
		if(Session.current().get("email")==null || Session.current().get("username")==null) {
			Logger.info("No valid session exists..redirecting to login page");
			Logger.info("Id value is-->" + params.get("id"));
			
			redirect("/login");
		}
        else {
        	Logger.info("valid session exists..");
        }	
    }
    
    public static void facebookLogout() {
    	Logger.info("##Give.facebookLogout##");    	
        Session.current().remove("username");
        Session.current().remove("name");
        FbGraph.destroySession();
        redirect("/login");
    }
    
}