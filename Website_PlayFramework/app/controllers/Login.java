package controllers;

import com.google.gson.JsonObject;

import play.Logger;
import play.cache.Cache;
import play.modules.facebook.FbGraph;
import play.modules.facebook.FbGraphException;
import play.mvc.Controller;
import play.mvc.Scope.Session;

public class Login extends Controller {

    public static void index() {
    	Logger.info("##in Login.index before render [checked] ##");
    	render();
    }
    
    public static void loginAsGuest() {
        // do useful things
        Session.current().put("email", params.get("email")); // put the email into the session (for the Secure module)
        Session.current().put("username", params.get("username")); // put the name into the session    
        Session.current().put("loginasguest", "true");
        Logger.info("After setting email and username in session");
    }
    
}