package exceptions;

/**
 * Created by IntelliJ IDEA.
 * User: parimisrinivas
 * Date: Feb 8, 2012
 * Time: 4:31:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class InvalidInputDataException extends Exception {
    public InvalidInputDataException() {
        super();
    }

    public InvalidInputDataException(String message) {
        super(message);
    }
}
