package models;

import java.sql.Date;
import java.util.ArrayList;

import javax.persistence.Entity;

import play.db.jpa.Model;

@Entity
public class Bookmarks extends Model{
	public String postedBy;
	public String emailAddress;
	public String mediaUrl;
	public int actualHeight;
	public int actualWidth;
	public ArrayList<String> comments = new ArrayList();
	public String websiteUrl;
	public Date postedTimeStamp;
	public long questionHeight;
	public long questionWidth;
	public int numberOfLikes;
	
	public Bookmarks(String postedBy, String email, String mediaUrl, int height, int width, String comment, String websiteUrl){
		this.postedBy = postedBy;
		this.emailAddress = email;
		this.mediaUrl = mediaUrl;
		this.actualHeight = height;
		this.actualWidth = width;
		comments.add(comment);
		this.websiteUrl = websiteUrl;
	}

	public void setQuestionHeight(long questionHeight) {
		this.questionHeight = questionHeight;
	}

	public void setQuestionWidth(long questionWidth) {
		this.questionWidth = questionWidth;
	}

	public String getPostedBy() {
		return postedBy;
	}

	public void setPostedBy(String postedBy) {
		this.postedBy = postedBy;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getMediaUrl() {
		return mediaUrl;
	}

	public void setMediaUrl(String mediaUrl) {
		this.mediaUrl = mediaUrl;
	}

	public int getActualHeight() {
		return actualHeight;
	}

	public void setActualHeight(int actualHeight) {
		this.actualHeight = actualHeight;
	}

	public int getActualWidth() {
		return actualWidth;
	}

	public void setActualWidth(int actualWidth) {
		this.actualWidth = actualWidth;
	}

	public ArrayList<String> getComments() {
		return comments;
	}

	public void setComments(String comment) {
		this.comments.add(comment);
	}

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}

	public Date getPostedTimeStamp() {
		return postedTimeStamp;
	}

	public void setPostedTimeStamp(Date postedTimeStamp) {
		this.postedTimeStamp = postedTimeStamp;
	}

	public long getQuestionHeight() {
		return questionHeight;
	}

	public long getQuestionWidth() {
		return questionWidth;
	}

	
	public int getNumberOfLikes() {
		return numberOfLikes;
	}

	public void setNumberOfLikes(int numberOfLikes) {
		this.numberOfLikes = numberOfLikes;
	}

	@Override
	public String toString() {
		return "Bookmarks [postedBy=" + postedBy + ", emailAddress="
				+ emailAddress + ", mediaUrl=" + mediaUrl + ", actualHeight="
				+ actualHeight + ", actualWidth=" + actualWidth + ", comments="
				+ comments + ", websiteUrl=" + websiteUrl
				+ ", postedTimeStamp=" + postedTimeStamp + ", questionHeight="
				+ questionHeight + ", questionWidth=" + questionWidth
				+ ", numberOfLikes=" + numberOfLikes + "]";
	}
}
