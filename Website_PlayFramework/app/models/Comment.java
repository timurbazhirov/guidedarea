package models;

import java.sql.Date;
import javax.persistence.Entity;
import play.db.jpa.Model;

@Entity
public class Comment extends Model{
    public String question;
    public String friendsEmailAddresses;
    public String commentorEmailAddress;
    public String commentorName;
    public	Date postedTimeStamp;
    public String hash;
    public String firstProd;
    public String seconProd;
    public String thirdProd;
    
    public Comment(String question, String friendsEmailAddresses, String hash, String commentorEmailAddress, String commentorName, String first, String second, String third) {
    	this.question = question;
    	this.friendsEmailAddresses = friendsEmailAddresses;
    	this.hash = hash;
    	this.commentorEmailAddress = commentorEmailAddress;
    	this.commentorName = commentorName;
    	this.firstProd = first;
    	this.seconProd = second;
    	this.thirdProd = third;
    }
    
	
	public String getFirstProd() {
		return firstProd;
	}


	public void setFirstProd(String firstProd) {
		this.firstProd = firstProd;
	}


	public String getSeconProd() {
		return seconProd;
	}


	public void setSeconProd(String seconProd) {
		this.seconProd = seconProd;
	}


	public String getThirdProd() {
		return thirdProd;
	}


	public void setThirdProd(String thirdProd) {
		this.thirdProd = thirdProd;
	}


	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
		
	public String getFriendsEmailAddresses() {
		return friendsEmailAddresses;
	}
	public void setFriendsEmailAddresses(String friendsEmailAddresses) {
		this.friendsEmailAddresses = friendsEmailAddresses;
	}
	public Date getPostedTimeStamp() {
		return postedTimeStamp;
	}
	public void setPostedTimeStamp(Date postedTimeStamp) {
		this.postedTimeStamp = postedTimeStamp;
	}

	public String getCommentorEmailAddress() {
		return commentorEmailAddress;
	}

	public void setCommentorEmailAddress(String commentorEmailAddress) {
		this.commentorEmailAddress = commentorEmailAddress;
	}

	public String getCommentorName() {
		return commentorName;
	}

	public void setCommentorName(String commentorName) {
		this.commentorName = commentorName;
	}


	@Override
	public String toString() {
		return "Comment [question=" + question + ", friendsEmailAddresses="
				+ friendsEmailAddresses + ", commentorEmailAddress="
				+ commentorEmailAddress + ", commentorName=" + commentorName
				+ ", postedTimeStamp=" + postedTimeStamp + ", hash=" + hash
				+ ", firstProd=" + firstProd + ", seconProd=" + seconProd
				+ ", thirdProd=" + thirdProd + "]";
	}
}
