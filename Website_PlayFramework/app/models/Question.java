package models;

import play.db.jpa.Model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;

/**
 * Created by IntelliJ IDEA.
 * User: parimi
 * Date: Feb 8, 2012
 * Time: 11:09:38 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class Question extends Model{
	public String question;
	public String friendsEmailAddresses;
	public String firstProd;
	public String seconProd;
	public String thirdProd;
	public String createdBy;
	public String creatorEmailAddress;
	public Date postedTimeStamp;
	public String hash;
	
	public Question() {}
	public Question(String question, String friendsEmailAddresses,
			String firstProd, String seconProd, String thirdProd,
			String createdBy, String creatorEmailAddress,String hash) {
		super();
		this.question = question;
		this.friendsEmailAddresses = friendsEmailAddresses;
		this.firstProd = firstProd;
		this.seconProd = seconProd;
		this.thirdProd = thirdProd;
		this.createdBy = createdBy;
		this.creatorEmailAddress = creatorEmailAddress;
		this.hash = hash;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getFriendsEmailAddresses() {
		return friendsEmailAddresses;
	}

	public void setFriendsEmailAddresses(String friendsEmailAddresses) {
		this.friendsEmailAddresses = friendsEmailAddresses;
	}

	public String getFirstProd() {
		return firstProd;
	}

	public void setFirstProd(String firstProd) {
		this.firstProd = firstProd;
	}

	public String getSeconProd() {
		return seconProd;
	}

	public void setSeconProd(String seconProd) {
		this.seconProd = seconProd;
	}

	public String getThirdProd() {
		return thirdProd;
	}

	public void setThirdProd(String thirdProd) {
		this.thirdProd = thirdProd;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatorEmailAddress() {
		return creatorEmailAddress;
	}

	public void setCreatorEmailAddress(String creatorEmailAddress) {
		this.creatorEmailAddress = creatorEmailAddress;
	}

	public Date getPostedTimeStamp() {
		return postedTimeStamp;
	}

	public void setPostedTimeStamp(Date postedTimeStamp) {
		this.postedTimeStamp = postedTimeStamp;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	@Override
	public String toString() {
		return "Question [question=" + question + ", friendsEmailAddresses="
				+ friendsEmailAddresses + ", firstProd=" + firstProd
				+ ", seconProd=" + seconProd + ", thirdProd=" + thirdProd
				+ ", createdBy=" + createdBy + ", creatorEmailAddress="
				+ creatorEmailAddress + ", postedTimeStamp=" + postedTimeStamp
				+ ", hash=" + hash + "]";
	}
}
