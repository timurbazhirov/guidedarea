package services;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import org.apache.commons.codec.binary.Hex;

/**
 * Created by IntelliJ IDEA.
 * User: parimisrinivas
 * Date: Feb 8, 2012
 * Time: 4:35:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class HashGenerator {
    public static String generateHash(String emailAddress, String adviceId, String date)  {
    	String result=null;
    	try {
	    	Random rand = new Random();
	    	String randString = new StringBuffer().append(emailAddress).append(adviceId).append(date).append(rand.toString()).toString();
	    	
	    	MessageDigest messageDigest;
			messageDigest = MessageDigest.getInstance("MD5");
	    	messageDigest.reset();
	    	messageDigest.update(randString.getBytes(Charset.forName("UTF8")));
	    	final byte[] resultByte = messageDigest.digest();
	    	result = new String(Hex.encodeHex(resultByte));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
    }
}
