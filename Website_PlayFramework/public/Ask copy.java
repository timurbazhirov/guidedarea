package controllers;

import play.*;
import play.modules.facebook.FbGraph;
import play.modules.facebook.FbGraphException;
import play.mvc.*;
import play.mvc.Scope.Session;
import services.HashGenerator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import models.*;

public class Ask extends Controller {

    public static void index() {
		Logger.info("##Ask.index##");     	
        render();
    }
    
    public static void getbookmarks() {
		Logger.info("##Ask.getbookmarks##"); 
    	
    	List<Bookmarks> listOfBookmarks = Bookmarks.find("emailAddress = ? order by postedTime desc",Session.current().get("email")).fetch(3);
    	for(int index=0; index < listOfBookmarks.size() ; index++) {
    		Bookmarks obj = listOfBookmarks.get(index);
    		
    		double heightInDouble = new Double(obj.getActualHeight()).doubleValue();
    		double widthInDouble  = new Double(obj.getActualWidth()).doubleValue();
    		double aspect = widthInDouble/heightInDouble;
    		
    		if (widthInDouble > heightInDouble) {
   	         	obj.setQuestionWidth(200);
   	         	obj.setQuestionHeight(Math.round(200 / aspect));
    		}
    		else {
    			obj.setQuestionHeight(220);
    			obj.setQuestionWidth(Math.round(220 * aspect));
    		}
    	}
    	
    	//Convert into JSON.
    	Gson gson = new Gson();
    	String jsonStr = gson.toJson(listOfBookmarks);
    	Logger.info("JSON output-->" + jsonStr);
    	renderJSON(jsonStr);
    }
    
    public static void save(String question, String friends, String firstprod, String seconprod, String thirdprod) {
		Logger.info("##Ask.save##"); 
    	Logger.info("In Ask/save method");
        
        Logger.info("second Product-->" + seconprod);
        Logger.info("Third product-->" + thirdprod);
        Logger.info("username", Session.current().get("username"));
        Logger.info("user emailadd", Session.current().get("username"));

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        
        String hash = HashGenerator.generateHash(Session.current().get("username"),question, dateFormat.format(date));
        
        //Persist in database.
		Question newQuestion = new Question(question,friends,firstprod, seconprod,thirdprod,
				Session.current().get("username"),
				Session.current().get("email"),
				hash
				).save();
		
        renderJSON(hash);
    }
    
    public static void facebookLogin() {
		Logger.info("##Ask.facebookLogin##"); 
        try {
            JsonObject profile = FbGraph.getObject("me"); // fetch the logged in user
            String email = profile.get("email").getAsString(); // retrieve the email
            String name = profile.get("name").getAsString();
            
            Logger.info("Email address--> %s", email);
            Logger.info("Name -->%s", name);
            
            // do useful things
            Session.current().put("email", email); // put the email into the session (for the Secure module)
            Session.current().put("username", name); // put the name into the session 
            
            

        } catch (FbGraphException fbge) {
            flash.error(fbge.getMessage());
            if (fbge.getType() != null && fbge.getType().equals("OAuthException")) {
                Session.current().remove("username");
                Session.current().remove("email");
            }
        }
        index();
    }

    public static void facebookLogout() {
		Logger.info("##Ask.facebookLogout##"); 
    	Session.current().remove("username");
        Session.current().remove("name");
        FbGraph.destroySession();
        redirect("/login");
    }
    
    @Before(only={"index","getbookmarks","save"})
    static void checkAuthentification() {
		Logger.info("##Ask.checkAuthentification##");     	
		Logger.info("checking for a valid session-->");
        
		//Check if the username retrieved from Facebook exists in the session
		if(Session.current().get("email")==null || Session.current().get("username")==null) {
			Logger.info("No valid session exists..redirecting to login page");
			redirect("/login");			
		}
        else {
        	Logger.info("valid session exists..");
        }	
    }
}