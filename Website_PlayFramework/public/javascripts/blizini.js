    window.fbAsyncInit = function() {
        FB.init({appId: '249030838518928', status: true, cookie: true, xfbml: true});
        
        /* All the events registered */
        FB.Event.subscribe('auth.login', function(response) {
            // do something with response
            _gaq.push(['_trackEvent', 'Ask', 'Facebook', 'Login using Facebook id']);    		
            window.location = "@{Ask.facebookLogin()}";
        });
        FB.Event.subscribe('auth.logout', function(response) {
        	   // do something with response
            window.location = "/asklogout";
        });
    
        FB.getLoginStatus(function(response) {
        	if (response.session) {
                // logged in and connected user, someone you know
        		window.location = "@{Ask.facebookLogin()}";
            }
        });
    };
    (function() {
        var e = document.createElement('script');
        e.type = 'text/javascript';
        e.src = document.location.protocol +
            '//connect.facebook.net/en_US/all.js';
        e.async = true;
        document.getElementById('fb-root').appendChild(e);
    }());
    
    function savequestion() {
        _gaq.push(['_trackEvent', 'Ask', 'save', 'Save a question']);    		

    	console.log("in savequestion");
    	var images = [];   	
    	
      	$('img[id="selimage"]').each(function(i) {var imageobject = 
      		{
      			mediaurl: $(this).attr('src'),
      			height:$(this).attr('actualheight'),
      			width: $(this).attr('actualwidth'),
      			websiteurl: $(this).attr('websiteurl') 
      		};
      		images[i] = JSON.stringify(imageobject);
      		});
  
      	console.log("images[0]-->" + images[0]);
    	console.log("images[1]-->" + images[1]);
    	console.log("images[2]-->" + images[2]);

    	$.ajax({
    	    type: 'POST',
    	    url: '/Asksave',
    	    data: {
    	    	question: document.getElementById('textarea1').value,
    	    	friends: document.getElementById('textarea2').value,
    	    	firstprod: images[0],
    			seconprod: images[1],
    			thirdprod: images[2]
    	    },
    	    dataType: 'html',
    	    complete: function(xhr, textStatus) {
    	        console.log("textstatus is-->" + textStatus);
    	    },
    	    success: function(json) {
    	    	console.log('json output-->'+json);
    	    	document.getElementById('successdiv').removeAttribute('class');
    	        document.getElementById('successdiv').setAttribute('class','row');
    	        document.getElementById('successmessage').innerHTML='You have successfully posted the question. Your friends can add comments at <a>http://blizit.blizini.com/give/'+json +'</a>';	
    	    }
    	});
    }
	
    function getimages() {
        _gaq.push(['_trackEvent', 'Ask', 'get', 'Retrieve the last 3 images from database']);    		
    	console.log("in getimages");	
    	
    	$.ajax({
    	    type: 'POST',
    	    url: '/getbookmarks',
    	    dataType: 'json',
    	    complete: function(xhr, jsonstr) {
    	        console.log("textstatus is-->" + jsonstr);
    	    },
    	    success: function(json) {
    	    	if ( json.length ) {
    	    		var image1 = document.createElement('img');
    	    		image1.setAttribute('id','selimage');    	    		
    	    		image1.setAttribute('align','middle');
    	    		image1.setAttribute('width',json[0].questionWidth);
    	    		image1.setAttribute('height',json[0].questionHeight);
    	    		image1.setAttribute('src',json[0].mediaUrl);
    	    		image1.setAttribute('websiteurl',json[0].websiteUrl);
    	    		image1.setAttribute('actualheight',json[0].actualHeight);
    	    		image1.setAttribute('actualwidth',json[0].actualWidth);

    	    		var firstdiv = document.getElementById('firstimage');
    	    		firstdiv.appendChild(image1);
    	    		
    	    		var image2 = document.createElement('img');
    	    		image2.setAttribute('id','selimage');    	    		
    	    		image2.setAttribute('align','middle');
    	    		image2.setAttribute('width',json[1].questionWidth);
    	    		image2.setAttribute('height',json[1].questionHeight);
    	    		image2.setAttribute('src',json[1].mediaUrl);
    	    		image2.setAttribute('websiteurl',json[1].websiteUrl);
    	    		image2.setAttribute('actualheight',json[1].actualHeight);
    	    		image2.setAttribute('actualwidth',json[1].actualWidth);
    	    		
    	    		var seconddiv = document.getElementById('secondimage');
    	    		seconddiv.appendChild(image2);
    	    		
    	    		var image3 = document.createElement('img');
    	    		image3.setAttribute('id','selimage');    	    		
    	    		image3.setAttribute('align','middle');
    	    		image3.setAttribute('width',json[2].questionWidth);
    	    		image3.setAttribute('height',json[2].questionHeight);
    	    		image3.setAttribute('src',json[2].mediaUrl);
    	    		image3.setAttribute('websiteurl',json[2].websiteUrl);
    	    		image3.setAttribute('actualheight',json[2].actualHeight);
    	    		image3.setAttribute('actualwidth',json[2].actualWidth);
    	    		
    	    		var thirddiv = document.getElementById('thirdimage');
    	    		thirddiv.appendChild(image3);    		
    	    	}
            }
    	});
    }
