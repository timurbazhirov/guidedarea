var currSlide = 0;
var slideTimer;
var scrolling = true;
var played = false;;
var playing = false;
var sliding = false;
var containerScrollTop = 0;
var draggingBar = false;
var scrollDragStart = 0;
var scrollBarStart = 0;
var dragOffset = 0;
function loadCurrentAudio() {
    var filename = $('.slide').first().attr('data-audio');
    $('#sound')[0].src = '/audio/' + filename;
    $('#sound')[0].load();
    if (playing) {
        $('#sound')[0].play();
        clearTimeout(slideTimer);
    }
}
function prevSlide() {
    if(sliding) return;
    sliding = true;
    $('#slider_sound').addClass('hidden');
    $('.slide').removeClass('current');
    setTimeout(function(){
        $('#slider_sub').prepend($('#slider .slide').last()).css('left', '-920px').animate({
            left: 0
        }, function(){
            $('.slide').first().addClass('current');
            $('#slider_sound').removeClass('hidden');
            sliding = false;
            loadCurrentAudio();         
        });
    }, 200);
}
function nextSlide() {
    if(sliding) return;
    sliding = true;
    $('#slider_sound').addClass('hidden');
    $('.slide').removeClass('current');
    setTimeout(function(){
        $('#slider_sub').animate({
            left: -920
        }, function(){
            $(this).append($(this).find('.slide').first()).css('left', '0');
            $('.slide').first().addClass('current');
            played = false;
            $('#slider_sound').removeClass('hidden');
            sliding = false;
            loadCurrentAudio();
        });
    }, 200);
}
function scrollFunc(e) {
    e = e ? e : window.event;
    var direction = wheelData = e.detail ? e.detail * -1 : e.wheelDelta / 40;
    containerScrollTop += direction;
    if (containerScrollTop > 0) {
       containerScrollTop = 0;
    } else if (containerScrollTop < ($('.scroll_container').first().height() - $('.scroll_container p').first().height())) {
        containerScrollTop = $('.scroll_container').first().height() - $('.scroll_container p').first().height();
    }
    $('.scroll_bar').first().css({
        top: -1 * containerScrollTop * ($('.scroll_container').first().height() + 10 - $('.scroll_bar').first().height()) / $('.scroll_container p').first().height()
    })
    $('.scroll_container p').first().css({
        top: containerScrollTop
    })
    cancelEvent(e);
}
function cancelEvent(e)
{
  e = e ? e : window.event;
  if(e.stopPropagation)
    e.stopPropagation();
  if(e.preventDefault)
    e.preventDefault();
  e.cancelBubble = true;
  e.cancel = true;
  e.returnValue = false;
  return false;
}

$(document).ready(function(){
    $('#slider').append('<div id="slideLeft"></div><div id="slideRight"></div>');
    $('#slideLeft').click(function(){
        window.clearInterval(slideTimer);
        prevSlide();
        slideTimer = window.setInterval('nextSlide()', 4000);
    })
    $('#slideRight').click(function(){
        window.clearInterval(slideTimer);
        nextSlide();
        slideTimer = window.setInterval('nextSlide()', 4000);
    })
    loadCurrentAudio();
    slideTimer = window.setInterval('nextSlide()', 4000);
    $('.slide').first().addClass('current');
    $('#slider').mousemove(function(e){
          var offset = e.pageX - $('#slider').offset().left;
          if (offset > 87 && offset < 384) {
                window.clearInterval(slideTimer);
                scrolling = false;
          }
    })
    $('#slider').hover(function(){
    }, function(){
        if (!scrolling && !playing) {
            slideTimer = window.setInterval('nextSlide()', 4000);
            scrolling = true;
        }
    });
    $('.scroll_container').each(function(){
        $(this)[0].addEventListener('mousewheel', scrollFunc, false);  
    })
    $('#sound')[0].addEventListener('ended', function(){
        nextSlide();
        slideTimer = window.setInterval('nextSlide()', 4000);
    })
    $('.slider_popup').click(function(){
        if (!playing) {
            playing = true;
            $('#slider_sound').addClass('playing');
            $('#sound')[0].play();
        }        
    })
    $('#slider_play_mute').click(function(){
        if (playing) {
            playing = false;
            $('#slider_sound').removeClass('playing');
            $('#sound')[0].pause();
            $('#sound')[0].currentTime = 0;
        } else {
            playing = true;
            $('#slider_sound').addClass('playing');
            $('#sound')[0].play();  
            clearTimeout(slideTimer);        
        }
    })
    $('.scroll_bar').each(function(){
        containerHeight = $(this).parents('.scroll_container').height();
        var newHeight = 6 + (containerHeight * containerHeight) / $(this).parents('.scroll_container').find('p').height();
        if (newHeight > containerHeight) {
            $(this).hide();
        } else {
            $(this).css({
                height: newHeight
            })
        }
    })
    $('.scroll_bar').mousedown(function(e){
        draggingBar = true;
        dragOffset = e.clientY - $(this).offset().top;
        return false;
    })
    $(document).mouseup(function(e){
        draggingBar = false;
    })
    $(document).mousemove(function(e){
        if (draggingBar) {
            pos = (e.pageY - $('.scroll_bar').offset().top - dragOffset);
            // detail = 0; 
            if (pos > 0) {
                // detail = .5;
            } else {
                // detail = -.5;
            }
            evt = {detail: pos * 3};
            scrollFunc(evt);
        }
    })
})
