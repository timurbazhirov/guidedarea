delimiter $$

CREATE TABLE `bookmarks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `postedBy` varchar(100) NOT NULL,
  `emailAddress` varchar(45) NOT NULL,
  `mediaUrl` varchar(1000) NOT NULL,
  `actualHeight` int(11) NOT NULL,
  `actualWidth` int(11) NOT NULL,
  `comment` varchar(140) DEFAULT NULL,
  `websiteUrl` varchar(1000) NOT NULL,
  `postedTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `postedTimeStamp` date DEFAULT NULL,
  `questionHeight` bigint(20) NOT NULL,
  `questionWidth` bigint(20) NOT NULL,
  `comments` tinyblob,
  `numberOfLikes` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8$$

CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(200) NOT NULL,
  `hash` varchar(200) NOT NULL,
  `friendsEmailAddresses` varchar(300) NOT NULL,
  `firstProd` varchar(1000) DEFAULT NULL,
  `thirdProd` varchar(1000) DEFAULT NULL,
  `createdTimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createdBy` varchar(100) NOT NULL,
  `creatorEmailAddress` varchar(100) NOT NULL,
  `postedTimeStamp` date DEFAULT NULL,
  `seconProd` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index2` (`hash`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8$$



CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(200) NOT NULL,
  `friendsEmailAddresses` varchar(400) NOT NULL,
  `commentorEmailAddress` varchar(100) DEFAULT NULL,
  `commentorName` varchar(100) DEFAULT NULL,
  `hash` varchar(100) NOT NULL,
  `postedTimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `firstProd` varchar(5000) DEFAULT NULL,
  `seconProd` varchar(5000) DEFAULT NULL,
  `thirdProd` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8$$

